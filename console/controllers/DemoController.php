<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 12.01.2018
 * Time: 12:29
 */

namespace console\controllers;


use common\models\CheckList;
use common\models\Documents;
use common\models\Form;
use common\models\lang\ServiceStepLang;
use common\models\ServiceModule;
use common\models\ServiceStep;
use yii\console\Controller;

class DemoController extends Controller
{

    public function actionClear(){
        echo "-- Clear tables.\n";
        \Yii::$app->db->createCommand('TRUNCATE service_step_lang')->execute();
        \Yii::$app->db->createCommand('TRUNCATE service_step')->execute();
        \Yii::$app->db->createCommand('TRUNCATE service_lang')->execute();
        \Yii::$app->db->createCommand('TRUNCATE service')->execute();
        \Yii::$app->db->createCommand('TRUNCATE category_lang')->execute();
        \Yii::$app->db->createCommand('TRUNCATE category')->execute();
        \Yii::$app->db->createCommand('TRUNCATE municipal')->execute();
        \Yii::$app->db->createCommand('TRUNCATE city_lang')->execute();
        \Yii::$app->db->createCommand('TRUNCATE city')->execute();
        \Yii::$app->db->createCommand('TRUNCATE check_list_item_lang')->execute();
        \Yii::$app->db->createCommand('TRUNCATE check_list_item')->execute();
        \Yii::$app->db->createCommand('TRUNCATE check_list')->execute();
        \Yii::$app->db->createCommand('TRUNCATE fork_lang')->execute();
        \Yii::$app->db->createCommand('TRUNCATE fork')->execute();

        echo "OK!\n";
    }

    public function actionIndex(){
        echo "-- Install default data.\n";

        $this->actionFork();
        $this->actionCheckList();
        $this->actionCity();
        $this->actionMunicipal();
        $this->actionCategory();
        $this->actionService();
        $this->actionServiceStep();

        echo "OK! All data were installed.\n";
    }

    public function actionServiceStep(){
        echo "-- Install default service steps.\n";

        $models = [

            [
                'id' => 1,
                'step' => '0',
                'type' => '[1,2,3,4,5]',
                'status' => true,
                'position' => 0,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_FORK,
                'module_id' => json_encode([1,2,4])
            ],
            // 1
            [
                'id' => 2,
                'step' => '1',
                'type' => '[1]',
                'status' => true,
                'position' => 10,
                'service_id' => 1,
                'module_name' => null,
                'module_id' => null
            ],
            [
                'id' => 3,
                'step' => '1',
                'type' => '[2]',
                'status' => true,
                'position' => 10,
                'service_id' => 1,
                'module_name' => null,
                'module_id' => null
            ],
            [
                'id' => 4,
                'step' => '1',
                'type' => '[3]',
                'status' => true,
                'position' => 10,
                'service_id' => 1,
                'module_name' => null,
                'module_id' => null
            ],
            [
                'id' => 5,
                'step' => '1',
                'type' => '[4]',
                'status' => true,
                'position' => 10,
                'service_id' => 1,
                'module_name' => null,
                'module_id' => null
            ],
            [
                'id' => 6,
                'step' => '1',
                'type' => '[5]',
                'status' => true,
                'position' => 10,
                'service_id' => 1,
                'module_name' => null,
                'module_id' => null
            ],
            // 2
            [
                'id' => 7,
                'step' => '2',
                'type' => '[1,2,3,4,5]',
                'status' => true,
                'position' => 20,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_CHECK_LIST,
                'module_id' => json_encode(1),
            ],
            // 3
            [
                'id' => 8,
                'step' => '3',
                'type' => '[1]',
                'status' => true,
                'position' => 30,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_DOWNLOAD_FILE,
                'module_id' => json_encode('doc_pink-slip.zip'),
            ],
            [
                'id' => 9,
                'step' => '3',
                'type' => '[2,3,4,5]',
                'status' => true,
                'position' => 30,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_LOGIN,
                'module_id' => null,
            ],
            // 4
            [
                'id' => 10,
                'step' => '4',
                'type' => '[1]',
                'status' => true,
                'position' => 40,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_FORK,
                'module_id' => json_encode([2,3,4,5]),
            ],
            [
                'id' => 11,
                'step' => '4',
                'type' => '[2,3,4,5]',
                'status' => true,
                'position' => 40,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_FORM,
                'module_id' => json_encode(Form::FORM_PINK_SLIP),
            ],
            // 5
            [
                'id' => 12,
                'step' => '5',
                'type' => '[1,5]',
                'status' => true,
                'position' => 50,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_MUNICIPAL,
                'module_id' => json_encode([1,2]),
            ],
            [
                'id' => 13,
                'step' => '5',
                'type' => '[2]',
                'status' => true,
                'position' => 50,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_FORK,
                'module_id' => json_encode([3,4,5]),
            ],
            [
                'id' => 14,
                'step' => '5',
                'type' => '[3]',
                'status' => true,
                'position' => 50,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_FORK,
                'module_id' => json_encode([4,5]),
            ],
            [
                'id' => 15,
                'step' => '5',
                'type' => '[4]',
                'status' => true,
                'position' => 50,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_FORK,
                'module_id' => json_encode([5]),
            ],
            // 6
            [
                'id' => 16,
                'step' => '6',
                'type' => '[1]',
                'status' => true,
                'position' => 60,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_ADVICE_SERVICE,
                'module_id' => json_encode(2),
            ],
            [
                'id' => 17,
                'step' => '6',
                'type' => '[2,3,4]',
                'status' => true,
                'position' => 60,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_MUNICIPAL,
                'module_id' => json_encode([1,2]),
            ],
            [
                'id' => 18,
                'step' => '6',
                'type' => '[5]',
                'status' => true,
                'position' => 60,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_PAY,
                'module_id' => json_encode(200),
            ],
            // 7
            [
                'id' => 19,
                'step' => '7',
                'type' => '[2]',
                'status' => true,
                'position' => 70,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_PAY,
                'module_id' => json_encode(5),
            ],
            [
                'id' => 20,
                'step' => '7',
                'type' => '[3]',
                'status' => true,
                'position' => 70,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_PAY,
                'module_id' => json_encode(15),
            ],
            [
                'id' => 21,
                'step' => '7',
                'type' => '[4]',
                'status' => true,
                'position' => 70,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_PAY,
                'module_id' => json_encode(100),
            ],
            [
                'id' => 22,
                'step' => '7',
                'type' => '[5]',
                'status' => true,
                'position' => 70,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_SEND_REVIEW,
                'module_id' => json_encode([Documents::DOC_PASSPORT, Documents::DOC_MARRIAGE]),
            ],
            // 8
            [
                'id' => 23,
                'step' => '8',
                'type' => '[2]',
                'status' => true,
                'position' => 70,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_GET_DOCUMENT,
                'module_id' => json_encode(Form::FORM_PINK_SLIP),
            ],
            [
                'id' => 24,
                'step' => '8',
                'type' => '[3,4]',
                'status' => true,
                'position' => 80,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_SEND_REVIEW,
                'module_id' => json_encode([Documents::DOC_PASSPORT, Documents::DOC_MARRIAGE]),
            ],
            // 9
            [
                'id' => 25,
                'step' => '9',
                'type' => '[2,3]',
                'status' => true,
                'position' => 90,
                'service_id' => 1,
                'module_name' => ServiceModule::MODULE_ADVICE_SERVICE,
                'module_id' => json_encode(2),
            ],



            // Finish
            [
                'id' => 26,
                'step' => '0',
                'type' => '[1,2,3,4,5]',
                'status' => true,
                'position' => 100,
                'service_id' => 1,
                'module_name' => null,
                'module_id' => null,
            ],

            // Migration entry
            [
                'id' => 27,
                'step' => '0',
                'type' => '[1]',
                'status' => true,
                'position' => 10,
                'service_id' => 2,
                'module_name' => null,
                'module_id' => null,
            ],

        ];

        $langs = [
            $this->ServiceStepItem(1, '', '', '', 'en-US'),
            $this->ServiceStepItem(1, '', '', '', 'ru-RU'),
            $this->ServiceStepItem(2, 'Procedure', 'Procedure for free information', '', 'en-US'),
            $this->ServiceStepItem(2, 'Процедура', 'Процедура для бесплатной информации', '', 'ru-RU'),
            $this->ServiceStepItem(3, 'Procedure', 'Procedure for electronic documents', '', 'en-US'),
            $this->ServiceStepItem(3, 'Процедура', 'Процедура для электронных документов', '', 'ru-RU'),
            $this->ServiceStepItem(4, 'Procedure', 'Procedure for ', '', 'en-US'),
            $this->ServiceStepItem(4, 'Процедура', 'Процедура для ', '', 'ru-RU'),
            $this->ServiceStepItem(5, 'Procedure', 'Procedure for ', '', 'en-US'),
            $this->ServiceStepItem(5, 'Процедура', 'Процедура для ', '', 'ru-RU'),
            $this->ServiceStepItem(6, 'Procedure', 'Procedure for ', '', 'en-US'),
            $this->ServiceStepItem(6, 'Процедура', 'Процедура для ', '', 'ru-RU'),
            $this->ServiceStepItem(7, '', '', '', 'en-US'),
            $this->ServiceStepItem(7, '', '', '', 'ru-RU'),
            $this->ServiceStepItem(8, 'Download documents for filling', '', '', 'en-US'),
            $this->ServiceStepItem(8, 'Скачать необходимы документы', '', '', 'ru-RU'),
            $this->ServiceStepItem(9, 'Authorization', 'For further work with the system you need to log in', '', 'en-US'),
            $this->ServiceStepItem(9, 'Авторизация', 'Для дальнейшей работы с системой необходимо авторизоваться', '', 'ru-RU'),
            $this->ServiceStepItem(10, 'Make your immigration faster', '', '', 'en-US'),
            $this->ServiceStepItem(10, 'Сделайте свою иммграцию быстрее', '', '', 'ru-RU'),
            $this->ServiceStepItem(11, 'Fill the form', '', '', 'en-US'),
            $this->ServiceStepItem(11, 'Заполните форму', '', '', 'ru-RU'),
            $this->ServiceStepItem(12, 'Municipal service', '', '', 'en-US'),
            $this->ServiceStepItem(12, 'Муниципальные сервисы', '', '', 'ru-RU'),
            $this->ServiceStepItem(13, 'Make your immigration faster', '', '', 'en-US'),
            $this->ServiceStepItem(13, 'Сделайте свою иммграцию быстрее', '', '', 'ru-RU'),
            $this->ServiceStepItem(14, 'Make your immigration faster', '', '', 'en-US'),
            $this->ServiceStepItem(14, 'Сделайте свою иммграцию быстрее', '', '', 'ru-RU'),
            $this->ServiceStepItem(15, 'Make your immigration faster', '', '', 'en-US'),
            $this->ServiceStepItem(15, 'Сделайте свою иммграцию быстрее', '', '', 'ru-RU'),
            $this->ServiceStepItem(16, 'Make entry to the municipal service', '', '', 'en-US'),
            $this->ServiceStepItem(16, 'Забронируйте время в миграционном центре', '', '', 'ru-RU'),
            $this->ServiceStepItem(17, 'Municipal service', '', '', 'en-US'),
            $this->ServiceStepItem(17, 'Муниципальные сервисы', '', '', 'ru-RU'),
            $this->ServiceStepItem(18, 'Payment for services', 'Service cost 200 EURO', '', 'en-US'),
            $this->ServiceStepItem(18, 'Оплатите услуги', 'Стоимость услуги 200 евро', '', 'ru-RU'),
            $this->ServiceStepItem(19, 'Payment for services', 'Service cost 5 EURO', '', 'en-US'),
            $this->ServiceStepItem(19, 'Оплатите услуги', 'Стоимость услуги 5 евро', '', 'ru-RU'),
            $this->ServiceStepItem(20, 'Payment for services', 'Service cost 15 EURO', '', 'en-US'),
            $this->ServiceStepItem(20, 'Оплатите услуги', 'Стоимость услуги 15 евро', '', 'ru-RU'),
            $this->ServiceStepItem(21, 'Payment for services', 'Service cost 100 EURO', '', 'en-US'),
            $this->ServiceStepItem(21, 'Оплатите услуги', 'Стоимость услуги 100 евро', '', 'ru-RU'),
            $this->ServiceStepItem(22, 'Send documents for review to the curator', '', '', 'en-US'),
            $this->ServiceStepItem(22, 'Отправить документы на проверку куратору', '', '', 'ru-RU'),
            $this->ServiceStepItem(23, 'Check your documents', 'Check your documents and make correction if you need', '', 'en-US'),
            $this->ServiceStepItem(23, 'Проверьте свои документы', 'Проверьте свои документы и отредактируйте если требуется', '', 'ru-RU'),
            $this->ServiceStepItem(24, 'Send documents for review to the curator', '', '', 'en-US'),
            $this->ServiceStepItem(24, 'Отправить документы на проверку куратору', '', '', 'ru-RU'),
            $this->ServiceStepItem(25, 'Make entry to the municipal service', '', '', 'en-US'),
            $this->ServiceStepItem(25, 'Забронируйте время в миграционном центре', '', '', 'ru-RU'),
            $this->ServiceStepItem(26, 'Wait for your documents to arrive', 'The time for consideration of an application can be up to 6 months. Once your application has been reviewed, you will receive a notification of receipt of your visa at the mailing address', '', 'en-US'),
            $this->ServiceStepItem(26, 'Дождитесь прихода ваших документов', 'Срок рассмотрения заявки может доходить до 6 месяцев. Как только ваша заявка будет рассмотренна, вам на почтовый адресс прийдет оповещение о получении визы', '', 'ru-RU'),
            $this->ServiceStepItem(27, 'Make entry', 'Text for migration entry', '', 'en-US'),
            $this->ServiceStepItem(27, 'Забронировать вемя в миграционном центре', 'Текст процедуры получения', '', 'ru-RU'),
        ];

        foreach ($models as $item){
            $model = new \common\models\ServiceStep();
            $model->attributes = $item;
            $model->save();
        }
        foreach ($langs as $item){
            $model = new \common\models\lang\ServiceStepLang();
            $model->attributes = $item;
            $model->save();
        }

        echo "OK! Service steps were installed.\n";
    }

    public function actionFork(){
        echo "-- Install default forks.\n";

        $models = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
            ['id' => 4],
            ['id' => 5],
        ];

        $langs = [
            [
                'language' => 'en-US',
                'fork_id' => 1,
                'name' => 'Free information',
            ],[
                'language' => 'ru-RU',
                'fork_id' => 1,
                'name' => 'Бесплатная информация',
            ],[
                'language' => 'en-US',
                'fork_id' => 2,
                'name' => 'Electronic documents',
            ],[
                'language' => 'ru-RU',
                'fork_id' => 2,
                'name' => 'Электронные документы',
            ],[
                'language' => 'en-US',
                'fork_id' => 3,
                'name' => 'Electronic documents + online consultation',
            ],[
                'language' => 'ru-RU',
                'fork_id' => 3,
                'name' => 'Электронные документы + онлайн консультация',
            ],[
                'language' => 'en-US',
                'fork_id' => 4,
                'name' => 'Consulting',
            ],[
                'language' => 'ru-RU',
                'fork_id' => 4,
                'name' => 'Консультирование',
            ],[
                'language' => 'en-US',
                'fork_id' => 5,
                'name' => 'Minimum participation',
            ],[
                'language' => 'ru-RU',
                'fork_id' => 5,
                'name' => 'Минимальное участие',
            ],
        ];

        foreach ($models as $item){
            $model = new \common\models\Fork();
            $model->attributes = $item;
            $model->save();
        }
        foreach ($langs as $item){
            $model = new \common\models\lang\ForkLang();
            $model->attributes = $item;
            $model->save();
        }

        echo "OK! Forks were installed.\n";
    }

    public function actionCheckList(){
        echo "-- Install default checklist.\n";

        $model = new CheckList();
        $model->id = 1;
        $model->name = 'MVIS3';
        $model->save();

        $models = [
            ['id' => 1, 'list_id' => 1],
            ['id' => 2, 'list_id' => 1],
            ['id' => 3, 'list_id' => 1],
            ['id' => 4, 'list_id' => 1],
            ['id' => 5, 'list_id' => 1],
            ['id' => 6, 'list_id' => 1],
            ['id' => 7, 'list_id' => 1],
            ['id' => 8, 'list_id' => 1],
            ['id' => 9, 'list_id' => 1],
            ['id' => 10, 'list_id' => 1],
            ['id' => 11, 'list_id' => 1],
            ['id' => 12, 'list_id' => 1],
        ];

        $langs = [
            [ 'language' => 'en-US',
                'check_list_item_id' => 1,
                'value' => 'Copy of valid passport or other travel document with validity of at least three (3) months from the date of the submission of the application',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 1,
                'value' => 'Копия действительного паспорта или другого проездного документа со сроком действия не менее трех (3) месяцев с даты подачи заявки',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 2,
                'value' => 'Copy of the passport or other travel document, indicating the last arrival in the Republic and the relevant visa (if applicable)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 2,
                'value' => 'Копия паспорта или другого проездного документа с указанием последнего прибытия в Республику и соответствующей визы (если применимо)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 3,
                'value' => 'Copy of the Special Homogenous Identity Card (SHIC), or Certificate by the Embassy of Greece stating that the application for the SHIC is still under consideration',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 3,
                'value' => 'Копия специальной карты гомогенной идентификации (SHIC) или сертификат посольства Греции, в котором говорится, что заявка на SHIC все еще находится на рассмотрении',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 4,
                'value' => 'Letter of approval (where applicable)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 4,
                'value' => 'Письмо о допущении (если применимо)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 5,
                'value' => 'Original letter of guarantee issued by a bank or cooperative institution in Cyprus with validity of 10 years covering possible repatriation expenses. The amount depends on the country of origin.',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 5,
                'value' => 'Оригинальное гарантийное письмо, выданное банком или кооперативным учреждением на Кипре со сроком действия 10 лет, покрывающее возможные расходы на репатриацию. Сумма зависит от страны происхождения.',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 6,
                'value' => 'Certificate of Health Insurance for medical care that covers inpatient and outpatient care and transportation of corpse (Plan A)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 6,
                'value' => 'Свидетельство о медицинском страховании на медицинское обслуживание, которое охватывает стационарную и амбулаторную помощь и транспортировку трупа (план A)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 7,
                'value' => 'Copy of marriage certificate (where applicable)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 7,
                'value' => 'Копия свидетельства о браке (если применимо)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 8,
                'value' => 'Title deed or rental agreement of a house/apartment duly certified by the relevant president of the community (mukhtar)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 8,
                'value' => 'Титул или договор аренды дома / квартиры, должным образом удостоверенный соответствующим президентом общины (мухтар)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 9,
                'value' => 'Proof of stable or adequate income deriving from sources other than employment (i.e. pension, bank account\'s statement, interest from deposits, dividends etc)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 9,
                'value' => 'Доказательство наличия стабильного или адекватного дохода из источников, отличных от занятых (например, пенсия, отчет банковского счета, проценты от вкладов, дивиденды и т. Д.)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 10,
                'value' => 'Certificate from a banking institution in Cyprus or a bank account statement proving swifts from abroad',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 10,
                'value' => 'Свидетельство в банковском учреждении на Кипре или выписка по банковскому счету, подтверждающая быстрые выплаты из-за рубежа',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 11,
                'value' => 'Declaration of the hosting person for a suitable place of residence (where applicable)',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 11,
                'value' => 'Декларация принимающего лица для подходящего места жительства (если применимо)',
            ],[ 'language' => 'en-US',
                'check_list_item_id' => 12,
                'value' => 'Declaration of assumption of the expenses by a person or institution (where applicable). In cases where the declaration of assumption of the expenses is made by a person, proof of adequate income must be submitted for that person',
            ],[ 'language' => 'ru-RU',
                'check_list_item_id' => 12,
                'value' => 'Декларация о допуске расходов лицом или учреждением (если применимо). В случаях, когда декларация о допуске расходов производится лицом, должно быть представлено доказательство достаточного дохода для этого лица',
            ],
        ];

        foreach ($models as $item){
            $model = new \common\models\CheckListItem();
            $model->attributes = $item;
            $model->save();
        }
        foreach ($langs as $item){
            $model = new \common\models\lang\CheckListItemLang();
            $model->attributes = $item;
            $model->save();
        }


        echo "OK! Checklists were installed.\n";
    }

    public function actionService(){
        echo "-- Install default Services.\n";

        $models = [
            [
                'id' => 1,
                'icon' => 'list',
                'color' => '#da519c',
                'status' => true,
                'is_main' => true,
                'code' => 'PS'
            ],[
                'id' => 2,
                'icon' => 'bell',
                'color' => '#c6ce65',
                'status' => true,
                'is_main' => true,
                'code' => 'ETM'
            ],
        ];

        $langs = [
            [
                'language' => 'en-US',
                'service_id' => 1,
                'name' => 'Pink slip',
                'description' => 'Help you quickly make a pink slip for you and your family',
                'cost' => 'up to 200 €',
                'time' => 'up to 3 weeks',
            ],[
                'language' => 'ru-RU',
                'service_id' => 1,
                'name' => 'Пинк слип',
                'description' => 'Поможем вам быстрее сделать пинк слип для вас и вашей семьи',
                'cost' => 'до 200 €',
                'time' => 'до 3 недель',
            ],[
                'language' => 'en-US',
                'service_id' => 2,
                'name' => 'Entry to migration',
                'description' => 'We will arrange for you to write to the migration center at the earliest convenient time for you',
                'cost' => 'From 20 €',
                'time' => 'up to 2 working days',
            ],[
                'language' => 'ru-RU',
                'service_id' => 2,
                'name' => 'Запись в миграционный центр',
                'description' => 'Мы организуем для вас запись в миграционный центр в ближайшее удобное для вас время',
                'cost' => 'От 20 €',
                'time' => 'до 2х рабочих дней',
            ],
        ];

        foreach ($models as $item){
            $model = new \common\models\Service();
            $model->attributes = $item;
            $model->save();
        }
        foreach ($langs as $item){
            $model = new \common\models\lang\ServiceLang();
            $model->attributes = $item;
            $model->save();
        }


        echo "OK! Services were installed.\n";
    }

    public function actionCategory(){
        echo "-- Install default categories.\n";

        $models = [
            [
                'id' => 1,
                'icon' => '',
                'color' => '',
                'status' => true,
                'parent_id' => 0
            ], [
                'id' => 2,
                'icon' => '',
                'color' => '',
                'status' => true,
                'parent_id' => 0
            ],
        ];

        $langs = [
            [
                'language' => 'en-US',
                'category_id' => 1,
                'name' => 'Migration',
                'description' => 'Migration services',
            ],[
                'language' => 'ru-RU',
                'category_id' => 1,
                'name' => 'Иммиграция',
                'description' => 'Услуги для имигрантов',
            ],[
                'language' => 'en-US',
                'category_id' => 2,
                'name' => 'Comunal',
                'description' => 'Comunal services',
            ],[
                'language' => 'ru-RU',
                'category_id' => 2,
                'name' => 'Комунальные услуги',
                'description' => 'Различные услуги связанные с комунальнами направлениями',
            ],
        ];

        foreach ($models as $item){
            $model = new \common\models\Category();
            $model->attributes = $item;
            $model->save();
        }
        foreach ($langs as $item){
            $model = new \common\models\lang\CategoryLang();
            $model->attributes = $item;
            $model->save();
        }

        echo "OK! Categories were installed.\n";
    }

    public function actionCity(){
        echo "-- Install default cities.\n";
        $models = [
            [
                'id' => 1,
                'lat' => '34.6917358',
                'lng' => '32.9930904'
            ],
            [
                'id' => 2,
                'lat' => '34.9013685',
                'lng' => '33.5852154'
            ],
            [
                'id' => 3,
                'lat' => '34.772681',
                'lng' => '32.415501'
            ],
            [
                'id' => 4,
                'lat' => '35.1922475',
                'lng' => '33.3623828'
            ],

        ];
        $langs = [
            [
                'city_id' => 1,
                'name' => 'Limassol',
                'language' => 'en-US',
            ],[
                'city_id' => 1,
                'name' => 'Лимассол',
                'language' => 'ru-RU',
            ],[
                'city_id' => 2,
                'name' => 'Larnaca',
                'language' => 'en-US',
            ],[
                'city_id' => 2,
                'name' => 'Ларнака',
                'language' => 'ru-RU',
            ],[
                'city_id' => 3,
                'name' => 'Paphos',
                'language' => 'en-US',
            ],[
                'city_id' => 3,
                'name' => 'Пафос',
                'language' => 'ru-RU',
            ],[
                'city_id' => 4,
                'name' => 'Nicosia',
                'language' => 'en-US',
            ],[
                'city_id' => 4,
                'name' => 'Никосия',
                'language' => 'ru-RU',
            ],
        ];

        foreach ($models as $item){
            $model = new \common\models\City();
            $model->attributes = $item;
            $model->save();
        }
        foreach ($langs as $item){
            $model = new \common\models\lang\CityLang();
            $model->attributes = $item;
            $model->save();
        }

        echo "OK! Cities were installed.\n";

    }

    public function actionMunicipal(){
        echo "-- Install default municipal services.\n";

        $models = [
            [
                'phone'             => '+357 22 804486',
                'email'             => '',
                'name'              => 'migration',
                'website'           => 'moi.gov.cy',
                'logo'              => '',
                'address'           => 'Civil Registry and Migration Department, Chilonos, 1457 Nicosia',
                'lat'               => '35.1711674',
                'lng'               => '33.3522781',
                'work_time'         => 'Monday - Friday 08:00 - 15:00',
                'city_id'           => '2',
            ],[
                'phone'             => '+357 25 805641',
                'email'             => '',
                'name'              => 'migration',
                'website'           => 'moi.gov.cy',
                'logo'              => '',
                'address'           => '223, Franklin Roosevelt, Limassol, Cyprus',
                'lat'               => '34.6593433',
                'lng'               => '33.0046442',
                'work_time'         => 'Monday - Friday 07:30 - 14:30',
                'city_id'           => '1',
            ]
        ];

        foreach ($models as $item){
            $model = new \common\models\Municipal();
            $model->attributes = $item;
            $model->save();
        }

        echo "OK! Municipal services were installed.\n";

    }

    private function ServiceStepItem($step_id, $title, $text, $text_bottom, $lng){
        return [
            'language' => $lng,
            'service_step_id' => $step_id,
            'title' => $title,
            'text' => $text,
            'text_bottom' => $text_bottom,
        ];
    }
}