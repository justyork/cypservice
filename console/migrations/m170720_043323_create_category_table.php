<?php

use yii\db\Migration;

class m170720_043323_create_category_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%category}}', [
            'id'            => $this->primaryKey(),
            'icon'          => $this->string(255),
            'color'          => $this->string(255),
            'status'        => $this->boolean(),
            'parent_id'     => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }

}
