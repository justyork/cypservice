<?php

use yii\db\Migration;

class m170720_100617_create_service_table extends Migration {

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%service}}', [
            'id'            => $this->primaryKey(),
            'icon'          => $this->string(255),
            'color'          => $this->string(255),
            'status'        => $this->boolean(),
            'is_main'        => $this->boolean(),
            'category_id'   => $this->integer(),
            'code'          => $this->string(10),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-service-category_id',
            'service',
            'category_id',
            'category',
            'id'
        );

        $this->createIndex(
            'uniq-code',
            'service',
            'code',
            1
        );

    }

    public function down()
    {
        $this->dropTable('{{%service}}');
    }
}
