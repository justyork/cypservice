<?php

use yii\db\Migration;

/**
 * Class m180112_042117_create_category_lang_table
 */
class m180112_042117_create_category_lang_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%category_lang}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255),
            'description'   => $this->text(),
            'category_id'   => $this->integer(),
            'language'      => $this->string(10)
        ], $tableOptions);

        $this->addForeignKey(
            'idx-category_lang-category_id',
            'category_lang',
            'category_id',
            'category',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%category_lang}}');
    }
}
