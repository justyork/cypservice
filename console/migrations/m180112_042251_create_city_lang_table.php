<?php

use yii\db\Migration;

/**
 * Class m180112_042251_create_city_lang_table
 */
class m180112_042251_create_city_lang_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%city_lang}}', [
            'id'                => $this->primaryKey(),
            'name'              => $this->string(),
            'city_id'           => $this->integer(),
            'language'          => $this->string(10)
        ], $tableOptions);


        $this->addForeignKey(
            'idx-city_lang-city_id',
            'city_lang',
            'city_id',
            'city',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%city_lang}}');
    }
}
