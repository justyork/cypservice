<?php

use yii\db\Migration;

/**
 * Class m180112_035447_create_service_lang_table
 */
class m180112_035447_create_service_lang_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%service_lang}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255),
            'description'   => $this->text(),
            'time'          => $this->string(255),
            'cost'          => $this->string(255),
            'service_id'    => $this->integer(),
            'language'      => $this->string(10),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-service_lang-service_id',
            'service_lang',
            'service_id',
            'service',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%service_lang}}');
    }

}
