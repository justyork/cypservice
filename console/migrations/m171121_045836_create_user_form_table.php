<?php

use yii\db\Migration;

class m171121_045836_create_user_form_table extends Migration
{

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_form}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'value' => $this->text(),
            'user_id' => $this->integer()
        ], $tableOptions);


        $this->addForeignKey(
            'idx-user_form-user_id',
            'user_form',
            'user_id',
            'user',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%user_form}}');
    }
}
