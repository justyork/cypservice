<?php

use yii\db\Migration;

/**
 * Class m180112_035501_create_service_step_lang_table
 */
class m180112_035501_create_service_step_lang_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%service_step_lang}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(255),
            'text'              => $this->text(),
            'text_bottom'       => $this->text(),
            'service_step_id'   => $this->integer(),
            'language'          => $this->string(10)
        ], $tableOptions);

        $this->addForeignKey(
            'idx-service_step_lang-service_step_id',
            'service_step_lang',
            'service_step_id',
            'service_step',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%service_step_lang}}');
    }
}
