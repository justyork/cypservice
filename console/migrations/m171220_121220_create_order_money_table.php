<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m171220_121220_create_order_money_table
 */
class m171220_121220_create_order_money_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%order_money}}', [
            'id'                => $this->primaryKey(),
            'order_id'            => $this->integer(),
            'price'             => $this->double(),
            'user_id'           => $this->integer(),
            'status'           => $this->integer(),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-order_money-user_id',
            'order_money',
            'user_id',
            'user',
            'id'
        );

        $this->addForeignKey(
            'idx-order_money-order_id',
            'order_money',
            'order_id',
            'order',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order_money}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_121220_create_order_money_table cannot be reverted.\n";

        return false;
    }
    */
}
