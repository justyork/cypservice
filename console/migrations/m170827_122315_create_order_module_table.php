<?php

use yii\db\Migration;

class m170827_122315_create_order_module_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%order_module}}', [
            'id'                => $this->primaryKey(),
            'order_id'          => $this->integer(),
            'module_name'       => $this->string(255),
            'data'              => $this->text(),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-order_module-order_id',
            'order_module',
            'order_id',
            'order',
            'id'
        );

    }

    public function down()
    {
        $this->dropTable('{{%order_module}}');
    }
}
