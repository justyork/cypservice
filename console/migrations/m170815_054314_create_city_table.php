<?php

use yii\db\Migration;

class m170815_054314_create_city_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%city}}', [
            'id'                => $this->primaryKey(),
            'lat'               => $this->string(20),
            'lng'               => $this->string(20),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%city}}');
    }
}
