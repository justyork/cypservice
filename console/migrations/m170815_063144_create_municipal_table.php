<?php

use yii\db\Migration;

class m170815_063144_create_municipal_table extends Migration
{

    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%municipal}}', [
            'id'                => $this->primaryKey(),
            'phone'             => $this->string(255),
            'name'              => $this->string(255),
            'email'             => $this->string(255),
            'website'           => $this->string(255),
            'logo'              => $this->string(255),
            'address'           => $this->text(),
            'lat'               => $this->string(20),
            'lng'               => $this->string(20),
            'work_time'         => $this->text(),
            'city_id'           => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-municipal-city_id',
            'municipal',
            'city_id',
            'city',
            'id'
        );

    }

    public function down()
    {
        $this->dropTable('{{%municipal}}');
    }
}
