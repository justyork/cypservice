<?php

use yii\db\Migration;
use yii\db\Schema;

class m170808_091822_create_order_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%order}}', [
            'id'                => $this->primaryKey(),
            'number'            => $this->string(255),
            'status'            => $this->integer(),
            'user_id'           => $this->integer(),
            'service_id'        => $this->integer(),
            'type_id'           => $this->integer(),
            'active'            => $this->boolean(),
            'data'              => $this->text(),
            'created_at'        => $this->integer(),
            'updated_at'        => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'idx-order-user_id',
            'order',
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'idx-order-service_id',
            'order',
            'service_id',
            'service',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%order}}');
    }
}
