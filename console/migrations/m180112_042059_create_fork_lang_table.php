<?php

use yii\db\Migration;

/**
 * Class m180112_042059_create_fork_lang_table
 */
class m180112_042059_create_fork_lang_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%fork_lang}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255),
            'fork_id'       => $this->integer(),
            'language'      => $this->string(10)
        ], $tableOptions);

        $this->addForeignKey(
            'idx-fork_lang-fork_id',
            'fork_lang',
            'fork_id',
            'fork',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%fork_lang}}');
    }
}
