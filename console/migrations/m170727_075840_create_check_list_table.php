<?php

use yii\db\Migration;

class m170727_075840_create_check_list_table extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up(){
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%check_list}}', [
            'id'            => $this->primaryKey(),
            'name'         => $this->string(255),
        ], $tableOptions);

    }

    public function down(){
        $this->dropTable('{{%check_list}}');
    }
}
