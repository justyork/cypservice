<?php

use yii\db\Migration;
use yii\helpers\Html;

class m170726_153947_create_service_step_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%service_step}}', [
            'id'            => $this->primaryKey(),
            'step'          => $this->integer(),
            'type'          => $this->string(255),
            'status'        => $this->boolean(),
            'position'      => $this->integer(),
            'service_id'    => $this->integer(),
            'module_name'   => $this->string(255),
            'module_id'     => $this->text()
        ], $tableOptions);

        $this->addForeignKey(
            'idx-service_step-service_id',
            'service_step',
            'service_id',
            'service',
            'id'
        );
    }



    public function down()
    {
        $this->dropTable('{{%service_step}}');
    }


}
