<?php

use yii\db\Migration;

/**
 * Class m180112_035607_create_check_list_item_lang_table
 */
class m180112_035607_create_check_list_item_lang_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%check_list_item_lang}}', [
            'id'                    => $this->primaryKey(),
            'value'                 => $this->string(255),
            'check_list_item_id'    => $this->integer(),
            'language'              => $this->string(10)
        ], $tableOptions);

        $this->addForeignKey(
            'idx-check_list_item_lang-check_list_item_id',
            'check_list_item_lang',
            'check_list_item_id',
            'check_list_item',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%check_list_item_lang}}');
    }
}
