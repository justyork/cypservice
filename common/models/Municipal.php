<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "municipal".
 *
 * @property integer $id
 * @property string $phone
 * @property string $name
 * @property string $email
 * @property string $website
 * @property string $logo
 * @property string $address
 * @property string $lat
 * @property string $lng
 * @property string $work_time
 * @property integer $city_id
 *
 * @property City $city
 */
class Municipal extends \yeesoft\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipal';
    }

    public static function map(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'work_time'], 'string'],
            [['city_id'], 'integer'],
            [['phone', 'name', 'email', 'website', 'logo'], 'string', 'max' => 255],
            [['lat', 'lng'], 'string', 'max' => 20],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'phone' => Yii::t('app', 'Phone'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'website' => Yii::t('app', 'Website'),
            'logo' => Yii::t('app', 'Logo'),
            'address' => Yii::t('app', 'Address'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'work_time' => Yii::t('app', 'Work Time'),
            'city_id' => Yii::t('app', 'City ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @inheritdoc
     * @return MunicipalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MunicipalQuery(get_called_class());
    }

}
