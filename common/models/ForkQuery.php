<?php

namespace common\models;

use omgdef\multilingual\MultilingualTrait;

/**
 * This is the ActiveQuery class for [[Fork]].
 *
 * @see Fork
 */
class ForkQuery extends \yii\db\ActiveQuery
{
    use MultilingualTrait;

    /**
     * @inheritdoc
     * @return Fork[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Fork|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}
