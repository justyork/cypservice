<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 21.11.2017
 * Time: 13:46
 */

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\i18n\Formatter;

class MyFormatter extends Formatter{

    public function asColor($value){
        return Html::tag('div', '', [
            'style' => 'width: 20px; height: 20px; border-radius: 3px; border: 1px solid black; background: '.$value
        ]);
    }

    public function asStatus($value){

        return Html::tag('span', ($value ? Yii::t('yee', 'Published') : Yii::t('yee', 'Unpublish')), [
            'class' => 'label label-'.($value ? 'success' : 'default'),
            'style' => 'font-size: 85%'
        ]);
    }

}