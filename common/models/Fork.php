<?php

namespace common\models;

use common\models\lang\ForkLang;
use yeesoft\behaviors\MultilingualBehavior;
use yeesoft\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "fork".
 *
 * @property integer $id
 * @property string $name
 */
class Fork extends ActiveRecord
{
    public static function map()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function behaviors()
    {
        return [
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'langForeignKey' => 'fork_id',
                'tableName' => "{{%fork_lang}}",
                'attributes' => [
                    'name',
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fork}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return ForkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ForkQuery(get_called_class());
    }


}
