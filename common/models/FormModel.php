<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.12.2017
 * Time: 13:16
 */

namespace common\models;


use app\models\UserForm;
use yii\base\Model;

/**
 * Class FormModel
 * @package common\models
 *
 * @property UserForm[] $userForm
 */
class FormModel extends Model{

    private $_userForm;

    public function init(){
        parent::init();

        $attr = [];
        if($this->getUserForm())
            foreach ($this->getUserForm() as $key => $item) $attr[$key] = $item->value;

        $attr[$this->formName()] = $attr;
        $this->load($attr);
    }

    /**
     * @return bool
     */
    public function save(){
        if(!$this->validate()) return false;


        foreach ($this->attributes as $key => $val){
            if($val == null) continue;

            if(!isset($this->userForm[$key])){
                $this->_userForm[$key] = new UserForm();
                $this->_userForm[$key]->name = $key;
                $this->_userForm[$key]->user_id = \Yii::$app->user->id;
            }

            $this->_userForm[$key]->value = $val;
            $this->_userForm[$key]->save();
        }

        return true;
    }

    public function getUserForm(){
        if($this->_userForm == null){
            $model = UserForm::findByCurrentUser();

            foreach ($model as $item){
                $this->_userForm[$item->name] = $item;
            }
        }
        return $this->_userForm;
    }
}