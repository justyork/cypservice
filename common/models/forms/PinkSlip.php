<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 22.11.2017
 * Time: 13:59
 */

namespace common\models\forms;


use common\models\FormModel;
use common\models\traits\ListTrait;
use Yii;

class PinkSlip extends FormModel{

    use ListTrait;

    public $name;
    public $surname;
    public $middle_name;
    public $nationality;
    public $date_birth;
    public $place_birth;
    public $country;

    public $marital_status;
    public $number_of_marriage;
    public $travel_doc_no;
    public $travel_doc_place;
    public $travel_doc_expire;

    public $port_entry;
    public $date_entry;

    public $gender;

    public $residence_arc;
    public $residence_status;
    public $residence_date_issue;
    public $residence_date_expire;


    public $home_phone;
    public $office_phone;
    public $mobile_phone;
    public $fax;
    public $email;

    public $address_street;
    public $address_area;
    public $address_city;
    public $address_postal;

    public $abroad_street;
    public $abroad_city;
    public $abroad_country;

    public $cor_different;

    public $cor_address_street;
    public $cor_address_area;
    public $cor_address_city;
    public $cor_address_postal;

    public $residence_own_house;
    public $residence_rented_house;
    public $residence_other;

    public $income;

    public $family_members;

    public $income_name;
    public $income_country;
    public $income_amount;

    public $family_name;
    public $family_nationality;
    public $family_date_birth;
    public $family_place_birth;
    public $family_relation;
    public $family_sex;
    public $family_country;
    public $family_arc;
    public $family_document;
    public $family_document_expire;

    private $encode_fields = ['family_arc', 'family_country', 'family_date_birth', 'family_document', 'family_members', 'family_name', 'family_nationality', 'family_place_birth', 'family_relation', 'family_sex', 'family_document_expire'];

    public function rules(){
        return [
            [['name', 'surname', 'email', 'country', 'place_birth', 'date_birth', 'travel_doc_no', 'travel_doc_expire', 'travel_doc_place' ], 'required'],
            [['marital_status'], 'integer'],
            [['name', 'surname', 'middle_name', 'place_birth', 'date_birth', 'nationality', 'country', 'abroad_street', 'abroad_country', 'number_of_marriage', 'travel_doc_no', 'travel_doc_expire', 'travel_doc_place', 'port_entry', 'date_entry', 'gender', 'residence_arc', 'residence_date_expire', 'residence_date_issue', 'residence_other', 'residence_status', 'address_area', 'address_city', 'address_postal', 'address_street', 'abroad_country', 'abroad_street', 'abroad_city', 'cor_address_area', 'cor_address_city', 'cor_address_postal', 'cor_address_street', 'home_phone', 'office_phone', 'mobile_phone', 'fax'], 'string', 'max' => 255],
            ['email', 'email'],
            [['family_arc', 'family_country', 'family_date_birth', 'family_document', 'family_members', 'family_name', 'family_nationality', 'family_place_birth', 'family_relation', 'family_sex', 'family_document_expire'], 'string'],
            [['residence_rented_house', 'residence_own_house', 'cor_different'], 'boolean'],

        ];
    }


    public function attributeLabels(){
        return [
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'middle_name' => Yii::t('app', 'Maiden surname'),
            'nationality' => Yii::t('app', 'Nationality'),
            'date_birth' => Yii::t('app', 'Date of birth'),
            'place_birth' => Yii::t('app', 'Place of birth'),
            'country' => Yii::t('app', 'Country of usual residence'),
            'marital_status' => Yii::t('app', 'Marital status'),
            'number_of_marriage' => Yii::t('app', 'In case of a polygamous marriage, number of marriages in force'),
            'travel_doc_no' => Yii::t('app', 'No'),
            'travel_doc_place' => Yii::t('app', 'Place of issue'),
            'travel_doc_expire' => Yii::t('app', 'Date of expiry'),
            'port_entry' => Yii::t('app', 'Port of entry'),
            'date_entry' => Yii::t('app', 'Date of entry'),
            'gender' => Yii::t('app', 'Sex'),

            'residence_arc' => Yii::t('app', 'ARC'),
            'residence_status' => Yii::t('app', 'Residence Status'),
            'residence_date_issue' => Yii::t('app', 'Date of issue'),
            'residence_date_expire' => Yii::t('app', 'Date of expiry'),

            'home_phone' => Yii::t('app', 'Home phone no'),
            'office_phone' => Yii::t('app', 'Office phone no'),
            'mobile_phone' => Yii::t('app', 'Mobile'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'E-mail'),
            'address_street' => Yii::t('app', 'Street and street no'),
            'address_area' => Yii::t('app', 'Area'),
            'address_city' => Yii::t('app', 'City'),
            'address_postal' => Yii::t('app', 'Postal code'),
            'abroad_street' => Yii::t('app', 'Street and street no'),
            'abroad_city' => Yii::t('app', 'City'),
            'abroad_country' => Yii::t('app', 'Country'),
            'cor_address_street' => Yii::t('app', 'Street and street no'),
            'cor_address_area' => Yii::t('app', 'Area'),
            'cor_address_city' => Yii::t('app', 'City'),
            'cor_address_postal' => Yii::t('app', 'Postal code'),
            'residence_own_house' => Yii::t('app', 'Privately owned house'),
            'residence_rented_house' => Yii::t('app', 'Rented house or apartment'),
            'residence_other' => Yii::t('app', 'Other (please specify)'),
            'income' => Yii::t('app', 'Income'),
            'family_members' => Yii::t('app', 'Family members'),

            'family_name' => Yii::t('app', 'Full Name'),
            'family_nationality' => Yii::t('app', 'Nationality'),
            'family_date_birth' => Yii::t('app', 'Date of birth'),
            'family_place_birth' => Yii::t('app', 'Place of birth'),
            'family_relation' => Yii::t('app', 'Relation with the alien'),
            'family_sex' => Yii::t('app', 'Sex'),
            'family_country' => Yii::t('app', 'Country of residence'),
            'family_arc' => Yii::t('app', 'ARC (if applicable)'),
            'family_document' => Yii::t('app', 'No of the Travel Document'),
            'family_document_expire' => Yii::t('app', 'Expiry Date of the Travel Document'),

            'cor_different' => Yii::t('app', 'Distinguished address of correspondence'),
        ];
    }


    public function beforeValidate(){

        foreach ($this->encode_fields as $field)
            $this->$field = json_encode($this->$field);

        return parent::beforeValidate();
    }

    public function init(){
        parent::init();

        foreach ($this->encode_fields as $field)
            $this->$field = json_decode($this->$field);

    }
}