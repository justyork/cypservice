<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 12.01.2018
 * Time: 22:03
 */

namespace common\models;


use Yii;

class Documents
{

    const DOC_PASSPORT = 1;
    const DOC_MARRIAGE = 2;

    public static function map(){
        return[
            self::DOC_PASSPORT => Yii::t('app', 'Passport'),
            self::DOC_MARRIAGE => Yii::t('app', 'Marriage certificate'),

            
        ];

    }
}