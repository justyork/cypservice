<?php

namespace common\models;

use common\models\lang\ServiceLang;
use yeesoft\behaviors\MultilingualBehavior;
use Yii;
use yeesoft\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property string $time
 * @property string $cost
 * @property string $color
 * @property integer $status
 * @property integer $is_main
 * @property integer $category_id
 * @property mixed $link
 * @property string $code
 * @property boolean|string $formName
 *
 * @property Order[] $orders
 * @property Category $category
 * @property array $pricePerType
 * @property ServiceStep[] $serviceSteps
 */
class Service extends ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;


    private $_form_name;
    private $_price_per_type;


    public function behaviors()
    {
        return [
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'langForeignKey' => 'service_id',
                'tableName' => "{{%service_lang}}",
                'attributes' => [
                    'name', 'description', 'time', 'cost'
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * getTypeList
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_NOT_ACTIVE => Yii::t('yee', 'Unpublish'),
            self::STATUS_ACTIVE => Yii::t('yee', 'Published'),
        ];
    }

    /**
     * getStatusOptionsList
     * @return array
     */
    public static function getStatusOptionsList(){
        return [
            [self::STATUS_NOT_ACTIVE, Yii::t('yee', 'Unpublish'), 'default'],
            [self::STATUS_ACTIVE, Yii::t('yee', 'Published'), 'success']
        ];
    }

    public static function getList(){
        return self::map();
    }

    public static function map(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['category_id'], 'integer'],
            [['icon', 'color', 'name', 'time', 'cost'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['code'], 'string', 'max' => 10],
            [['is_main', 'status'], 'boolean'],
            [['code'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cost' => Yii::t('app', 'Cost'),
            'time' => Yii::t('app', 'Time'),
            'icon' => Yii::t('app', 'Icon'),
            'color' => Yii::t('app', 'Color'),
            'status' => Yii::t('app', 'Status'),
            'is_main' => Yii::t('app', 'Is Main'),
            'category_id' => Yii::t('app', 'Category ID'),
            'code' => Yii::t('app', 'Code'),
        ];
    }

    public function getLink(){
        return Url::to(['service/get', 'id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceSteps()
    {
        return $this->hasMany(ServiceStep::className(), ['service_id' => 'id'])->orderBy('position');
    }

    /**
     * Get service steps by type
     *
     * @param $type
     * @return array|\yii\db\ActiveRecord[]|ServiceStep[]
     */
    public function stepsByType($type){
        return $this->getServiceSteps()
            ->where(['LIKE', 'type', '['.$type.','])
            ->orWhere(['LIKE', 'type', '['.$type.']'])
            ->orWhere(['LIKE', 'type', ','.$type.','])
            ->orWhere(['LIKE', 'type', ','.$type.']'])
            ->all();
    }

    /**
     * Get form name
     *
     * @return bool|string
     */
    public function getFormName(){
        if($this->_form_name == null){
            $model = ServiceStep::find()->where(['module_name' => 'form', 'service_id' => $this->id])->one();
            if(!$model) $this->_form_name = false;

            $form = $model->module_id;
            if(!$form) $this->_form_name = false;
            $this->_form_name = $form;
        }

        return $this->_form_name;
    }

    /**
     * @inheritdoc
     * @return ServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceQuery(get_called_class());
    }


    /**
     * Get price in the service type
     *
     * @return array
     */
    public function getPricePerType(){
        if(!$this->_price_per_type){
            /** @var ServiceStep[] $steps */
            $steps = $this->getServiceSteps()->where(['module_name' => ServiceModule::MODULE_PAY])->all();

            foreach ($steps as $item){
                $types = json_decode($item->type);
                $price = $item->module_id;

                foreach ($types as $type)
                    $this->_price_per_type[$type] = $price;
            }
        }
        return $this->_price_per_type;
    }

}
