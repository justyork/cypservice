<?php

namespace common\models;

use common\models\lang\ServiceStepLang;
use yeesoft\behaviors\MultilingualBehavior;
use Yii;

/**
 * This is the model class for table "service_step".
 *
 * @property integer $id
 * @property integer $step
 * @property string $title
 * @property string $text
 * @property string $text_bottom
 * @property integer $status
 * @property integer $position
 * @property integer $service_id
 * @property string $module_name
 * @property mixed $input_view
 * @property string $module_id
 *
 * @property Service $service
 * @property string $type [varchar(255)]
 */
class ServiceStep extends \yeesoft\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'langForeignKey' => 'service_step_id',
                'tableName' => "{{%service_step_lang}}",
                'attributes' => [
                    'title', 'text', 'text_bottom'
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_step';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['title'], 'required'],
            [['step', 'status', 'position', 'service_id'], 'integer'],
            [['text', 'text_bottom'], 'string'],
            ['module_id', 'safe'],
            [['type', 'module_name', 'title'], 'string', 'max' => 255],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'step' => Yii::t('app', 'Step name'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'text_bottom' => Yii::t('app', 'Text Bottom'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'position' => Yii::t('app', 'Position'),
            'service_id' => Yii::t('app', 'Service ID'),
            'module_name' => Yii::t('app', 'Module Name'),
            'module_id' => Yii::t('app', 'Module ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }


    public function beforeSave($insert){
        $this->module_id = json_encode($this->module_id);


//        die($this->module_id);
        return parent::beforeSave($insert);
    }

    public function afterFind(){
        parent::afterFind();
        $this->module_id = json_decode($this->module_id);
    }


    public function getInput_view(){
        if($this->module_name == 'fork'){
            $data = [];
            foreach ($this->module_id as $item)
                $data[] = $item->id.':'.$item->name;
            return implode("\n", $data);
        }
    }

    public static function findPay($id, $type_id){
        $model = self::find()
            ->where(['module_name' => 'pay'])
            ->andWhere(['status' => 1])
            ->andWhere(['service_id' => $id])
            ->all();

        if(!$model) return false;

        foreach ($model as $item){
            if(in_array($type_id, json_decode($item->type, true)))
                return $item;
        }

    }

    /**
     * @inheritdoc
     * @return ServiceStepQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceStepQuery(get_called_class());
    }
}
