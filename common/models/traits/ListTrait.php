<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 13.01.2018
 * Time: 19:51
 */
namespace common\models\traits;

use Yii;

trait ListTrait
{

    public static function getMaritalStatusList(){
        return [
            1 => Yii::t('app', 'Married'),
            2 => Yii::t('app', 'Single'),
            3 => Yii::t('app', 'Divorced'),
            4 => Yii::t('app', 'Separated'),
            5 => Yii::t('app', 'Widowed'),
        ];
    }

    public static function getGenderList(){
        return [
            1 => Yii::t('app', 'Famale'),
            2 => Yii::t('app', 'Male'),
            3 => Yii::t('app', 'Other')
        ];
    }
}