<?php

namespace common\models;

use common\models\lang\CheckListItemLang;
use yeesoft\behaviors\MultilingualBehavior;
use Yii;

/**
 * This is the model class for table "check_list_item".
 *
 * @property integer $id
 * @property integer $list_id
 * @property string $value
 *
 * @property CheckList $list
 * @property CheckListItemLang[] $checkListItemLangs
 */
class CheckListItem extends \yeesoft\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'langForeignKey' => 'check_list_item_id',
                'tableName' => "{{%check_list_item_lang}}",
                'attributes' => [
                    'value',
                ]
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_list_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['value', 'required'],
            ['value', 'string', 'max' => 255],
            [['list_id'], 'integer'],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => CheckList::className(), 'targetAttribute' => ['list_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'list_id' => Yii::t('app', 'List ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(CheckList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckListItemLangs()
    {
        return $this->hasMany(CheckListItemLang::className(), ['check_list_item_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return CheckListItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CheckListItemQuery(get_called_class());
    }
}
