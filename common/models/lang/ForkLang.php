<?php

namespace common\models\lang;

use common\models\Fork;
use Yii;

/**
 * This is the model class for table "fork_lang".
 *
 * @property integer $id
 * @property string $name
 * @property integer $fork_id
 * @property string $language
 *
 * @property Fork $fork
 */
class ForkLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fork_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fork_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
            [['fork_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fork::className(), 'targetAttribute' => ['fork_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'fork_id' => Yii::t('app', 'Fork ID'),
            'language' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFork()
    {
        return $this->hasOne(Fork::className(), ['id' => 'fork_id']);
    }
}
