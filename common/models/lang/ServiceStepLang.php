<?php

namespace common\models\lang;

use common\models\ServiceStep;
use Yii;

/**
 * This is the model class for table "service_step_lang".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $text_bottom
 * @property integer $service_step_id
 * @property string $language
 *
 * @property ServiceStep $serviceStep
 */
class ServiceStepLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_step_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'text_bottom'], 'string'],
            [['service_step_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
            [['service_step_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceStep::className(), 'targetAttribute' => ['service_step_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'text_bottom' => Yii::t('app', 'Text Bottom'),
            'service_step_id' => Yii::t('app', 'Service Step ID'),
            'language' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceStep()
    {
        return $this->hasOne(ServiceStep::className(), ['id' => 'service_step_id']);
    }
}
