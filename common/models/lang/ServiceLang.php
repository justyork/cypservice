<?php

namespace common\models\lang;

use common\models\Service;
use Yii;

/**
 * This is the model class for table "service_lang".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $time
 * @property string $cost
 * @property integer $service_id
 * @property string $language
 *
 * @property Service $service
 * @property Service $service0
 */
class ServiceLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['service_id'], 'integer'],
            [['name', 'time', 'cost'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'time' => Yii::t('app', 'Time'),
            'cost' => Yii::t('app', 'Cost'),
            'service_id' => Yii::t('app', 'Service ID'),
            'language' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

}
