<?php

namespace common\models\lang;

use common\models\CheckListItem;
use Yii;

/**
 * This is the model class for table "check_list_item_lang".
 *
 * @property integer $id
 * @property string $value
 * @property integer $check_list_item_id
 * @property string $language
 *
 * @property CheckListItem $checkListItem
 */
class CheckListItemLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_list_item_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['check_list_item_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
            [['check_list_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => CheckListItem::className(), 'targetAttribute' => ['check_list_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'value' => Yii::t('app', 'Value'),
            'check_list_item_id' => Yii::t('app', 'Check List Item ID'),
            'language' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckListItem()
    {
        return $this->hasOne(CheckListItem::className(), ['id' => 'check_list_item_id']);
    }
}
