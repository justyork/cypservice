<?php

namespace common\models;

use yeesoft\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $number
 * @property integer $status
 * @property integer $user_id
 * @property integer $service_id
 * @property integer $type_id
 * @property integer $active
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Service $service
 * @property User $user
 * @property OrderModule[] $orderModules
 * @property float|int $paid
 * @property array|mixed $formattedStatus
 * @property string $numberFull
 * @property OrderMoney[] $orderMoneys
 */
class Order extends \yii\db\ActiveRecord
{

    const STATUS_PROCESS = 0;
    const STATUS_MODERATE = 1;
    const STATUS_COMPLETE = 9;


    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'user_id', 'service_id', 'type_id', 'active', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['number'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_PROCESS],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Number'),
            'status' => Yii::t('app', 'Status'),
            'user_id' => Yii::t('app', 'User ID'),
            'service_id' => Yii::t('app', 'Service ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'active' => Yii::t('app', 'Active'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderModules()
    {
        return $this->hasMany(OrderModule::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery|OrderMoney
     */
    public function getOrderMoneys()
    {
        return $this->hasMany(OrderMoney::className(), ['order_id' => 'id']);
    }

    /**
     * Find active order by current user and service
     *
     * @param $current_service
     * @return array|Order|null|\yii\db\ActiveRecord
     */
    public static function findForUserByService($current_service){
        return self::find()->where([
            'service_id' => $current_service,
            'user_id' => Yii::$app->user->id,
            'active' => 1,
        ])->one();
    }

    /** Find all orders for user by service
     * @param $current_service
     * @return array|Order[]|\yii\db\ActiveRecord[]
     */
    public static function findAllByService($current_service){
        return self::find()->where([
            'service_id' => $current_service,
            'user_id' => Yii::$app->user->id,
        ])
            ->orderBy('status')
            ->all();
    }

    /**
     * Find active order
     *
     * @return Order|null|static
     */
    public static function findCurrentOrder(){
        $order_id = \Yii::$app->service->order;

        if($order_id == null){
            $model = new Order();
            $model->user_id = \Yii::$app->user->id;
            $model->type_id = \Yii::$app->service->type;
            $model->service_id = \Yii::$app->service->id;
            $model->active = 1;
            $model->generateNumber();
            $model->save();
            $model->isNewRecord = true;
        }
        else $model = Order::findOne($order_id);

        return $model;
    }

    /**
     * Generate order number
     */
    public function generateNumber(){
        $model = $this->service;
        $this->number = $model->code.Yii::$app->user->id.time();
    }

    /**
     * @return float|int
     */
    public function getPaid(){
        $total = 0;
        if($this->orderMoneys){
            foreach ($this->orderMoneys as $money)
                $total += $money->price;
        }

        return $total;
    }

    /**
     * Status list
     *
     * @return array|mixed
     */
    public static function StatusList(){
        return [
            self::STATUS_PROCESS => Yii::t('app', 'Active'),
            self::STATUS_MODERATE => Yii::t('app', 'Moderated'),
            self::STATUS_COMPLETE => Yii::t('app', 'Completed'),
        ];

    }

    /**
     * Formatted Status
     *
     * @return array|mixed
     */
    public function getFormattedStatus(){
        return self::StatusList()[$this->status];
    }

    /** Get order Number with status
     * @return string
     */
    public function getNumberFull(){
        return $this->number." ({$this->formattedStatus})";
    }

    public function getUserName(){
        return $this->user->username;
    }
    
    public function getServiceName(){
        return $this->service->name;
    }
}
