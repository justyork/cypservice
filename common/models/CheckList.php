<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "check_list".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CheckListItem[] $checkListItems
 */
class CheckList extends \yeesoft\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_list';
    }

    public static function map()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckListItems()
    {
        return $this->hasMany(CheckListItem::className(), ['list_id' => 'id']);
    }
    /**
     * @inheritdoc
     * @return CheckListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CheckListQuery(get_called_class());
    }
}
