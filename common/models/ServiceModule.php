<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 21.11.2017
 * Time: 18:43
 */

namespace common\models;


use Yii;
use yii\base\Model;
use yii\helpers\BaseHtml;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class ServiceModule{


    const MODULE_FORK = 'fork';
    const MODULE_CHECK_LIST = 'check-list';
    const MODULE_LOGIN = 'login';
    const MODULE_MUNICIPAL = 'municipal';
    const MODULE_CITY = 'select-city';
    const MODULE_FORM = 'form';
    const MODULE_PAY = 'pay';
    const MODULE_GET_DOCUMENT = 'get-document';
    const MODULE_ADVICE_SERVICE = 'advice-service';
    const MODULE_DATE_TIME = 'date-time';
    const MODULE_DOWNLOAD_FILE = 'download';
    const MODULE_SEND_REVIEW = 'review';
    const MODULE_CLOSE = 'close';


    private $module_name;
    private $form;
    private $field;
    private $hint;

    private $name = 'ServiceStep[module_id]';
    private $step_model;

    public function __construct($module_name, $step_id = null){
        if(!$step_id) $this->step_model = new ServiceStep();
        else $this->step_model = ServiceStep::findOne($step_id);
        $this->module_name = $module_name;
        $this->form = ActiveForm::widget();
    }

    public function getAttributeLabel(){
        return [
            'module_id' => Yii::t('app', 'Module data'),
        ];
    }

    public static function getList(){
        return [
            '' => Yii::t('app', '-- none --'),
            self::MODULE_FORK => Yii::t('app', 'Fork'),
            self::MODULE_CHECK_LIST => Yii::t('app', 'Check list'),
            self::MODULE_LOGIN => Yii::t('app', 'Check auth'),
            self::MODULE_MUNICIPAL => Yii::t('app', 'Select municipal service'),
//            self::MODULE_CITY => Yii::t('app', 'Select city'),
            self::MODULE_FORM => Yii::t('app', 'Form'),
            self::MODULE_PAY => Yii::t('app', 'Pay'),
            self::MODULE_GET_DOCUMENT => Yii::t('app', 'Generate document from form'),
            self::MODULE_ADVICE_SERVICE => Yii::t('app', 'Advice service'),
            self::MODULE_DOWNLOAD_FILE => Yii::t('app', 'Download file'),
//            self::MODULE_DATE_TIME => Yii::t('app', 'Date and time'),
            self::MODULE_SEND_REVIEW => Yii::t('app', 'Send to review'),
            self::MODULE_CLOSE => Yii::t('app', 'Close order'),
        ];
    }

    public function load(){
        if($this->module_name === "" ) return '';
        if(!method_exists($this, $this->funcName())) return '';
        $this->{$this->funcName()}();
        return $this->getField();
    }

    private function moduleDownload(){

        $this->field = '<script>$(\'.fancybox-ifr\').fancybox({
      \'width\'		: 900,
      \'height\'	: 600,
      \'type\'		: \'iframe\',
      \'autoScale\'    	: false
  });</script>';
        $this->field .= Html::activeTextInput($this->step_model, 'module_id', ['id' => 'download-file-field']);
        $this->field .= ' '.Html::a(Yii::t('app', 'File manager'), '/admin/filemanager/dialog.php?type=2&field_id=download-file-field', ['type' => 'button', 'class' => 'btn btn-primary fancybox-ifr']);


    }
    private function moduleFork(){

        $this->field = Html::activeDropDownList($this->step_model, 'module_id', Fork::map(), ['class' => 'form-control', 'multiple' => true]);
//        $this->step_model->module_id = $this->step_model->input_view;
//        $this->field = Html::activeTextarea($this->step_model, 'module_id', ['class' => 'form-control', 'rows' => 6]);
    }

    private function moduleCheckList(){
        $this->field = Html::activeDropDownList($this->step_model, 'module_id', CheckList::map(), ['class' => 'form-control', 'prompt' => Yii::t('app', '-- none --')]);
    }

    private function moduleMunicipal(){
        $this->field = Html::activeDropDownList($this->step_model, 'module_id', Municipal::map(), ['class' => 'form-control', 'multiple' => true]);
    }

    private function modulePay(){
        $this->field = Html::activeTextInput($this->step_model, 'module_id', ['class' => 'form-control']);
        $this->hint = Yii::t('app', 'Price for service');
    }

    private function moduleAdviceService(){
        $this->field = Html::activeDropDownList($this->step_model, 'module_id', Service::map(), ['class' => 'form-control', 'prompt' => Yii::t('app', '-- none --')]);
    }

    private function moduleForm(){
        $this->field = Html::activeDropDownList($this->step_model, 'module_id', Form::map(), ['class' => 'form-control', 'prompt' => Yii::t('app', '-- none --')]);
    }
    private function moduleReview(){
        $this->field = Html::activeDropDownList($this->step_model, 'module_id', Documents::map(), ['class' => 'form-control', 'multiple' => true]);
    }

    private function funcName(){
        $name_list = explode('-', $this->module_name);
        $name = 'module';
        foreach ($name_list as $item)
            $name .= ucfirst($item);

        return $name;
    }


    private function getField(){
        $ret = Html::beginTag('div', ['class' => 'form-group']);

        $ret .= Html::label($this->step_model->attributeLabels()['module_id']);
        $ret .= $this->field;
        if($this->hint != null)
            $ret .= Html::tag('div', $this->hint, ['class' => 'hint-block']);
        $ret .= Html::endTag('div');

        return $ret;
    }
    





}