<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "form".
 *
 * @property integer $id
 * @property string $name
 * @property string $f_name
 */
class Form
{

    const FORM_PINK_SLIP = 'pink-slip';


    public static function map()
    {
        return [
            self::FORM_PINK_SLIP => Yii::t('app', 'Pink slip'),
        ];
    }



}
