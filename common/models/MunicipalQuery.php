<?php

namespace common\models;

use omgdef\multilingual\MultilingualTrait;

/**
 * This is the ActiveQuery class for [[Municipal]].
 *
 * @see Municipal
 */
class MunicipalQuery extends \yii\db\ActiveQuery
{
    use MultilingualTrait;
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Municipal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Municipal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
