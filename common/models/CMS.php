<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 20.12.2017
 * Time: 17:29
 */

namespace common\models;


use Yii;
use yii\helpers\Html;
use ZipArchive;

class CMS
{

    public static function HourList(){
        return [
            8 => 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
        ];
    }

    public static function MinuteList(){
        return [
            0 => '00',
            5 => '05',
            10 => 10,
            15 => 15,
            20 => 20,
            25 => 25,
            30 => 30,
            35 => 35,
            40 => 40,
            45 => 45,
            50 => 50,
            55 => 55,
        ];
    }

    public static function dayList(){
        $arr = [];

        for($i = 1; $i <= 31; $i++)
            $arr[$i] = $i;

        return $arr;
    }

    public static function MonthList(){
        return [
            1 => Yii::t('app', 'January'),
            2 => Yii::t('app', 'February'),
            3 => Yii::t('app', 'March'),
            4 => Yii::t('app', 'April'),
            5 => Yii::t('app', 'May'),
            6 => Yii::t('app', 'June'),
            7 => Yii::t('app', 'Jule'),
            8 => Yii::t('app', 'August'),
            9 => Yii::t('app', 'September'),
            10 => Yii::t('app', 'October'),
            11 => Yii::t('app', 'November'),
            12 => Yii::t('app', 'December'),
        ];
    }


    public static function YearList(){
        $arr = [];

        for($i = date('Y') - 10; $i >= date('Y') - 100; $i--)
            $arr[$i] = $i;

        return $arr;
    }

    public static function IdToClass($name){
        $list = explode('-', $name);
        $new_name = '';
        foreach ($list as $item) {
            $new_name .= ucfirst($item);
        }
        return $new_name;
    }

    public static function NeedAuthAlert(){

        $ret = Html::beginTag('div', ['class' => 'alert alert-warning']);
        $ret .= Yii::t('app', 'You need to login').' '.Html::a(Yii::t('app', 'Login'), ['site/login'], ['style' => 'color: white']).' / '.Html::a(Yii::t('app', 'Registration'), ['site/registration'], ['style' => 'color: white']);
        $ret .= Html::endTag('div');

        return $ret;
    }

    public static function FormatPrice($price){
        return '€'.number_format($price);
    }

    public static function isHomePage() {
        $actualRoute = Yii::$app->controller->getRoute();

        list($controller, $actionId) = Yii::$app->createController('');
        $actionId = !empty($actionId) ? $actionId : $controller->defaultAction;
        $defaultRoute = $controller->getUniqueId() . '/' . $actionId;

        return $actualRoute === $defaultRoute;
    }

    public static function NeedToPay()
    {
        $ret = Html::beginTag('span', ['class' => 'badge badge-black']);
        $ret .= Yii::t('app', 'You need to pay for service') ;
        $ret .= Html::endTag('span');

        return $ret;
    }

    public static function OrderCompleted(){
        return Yii::t('app', 'Order already complete');
    }

    public static function checkDir($dir){
        if(!is_dir($dir)) mkdir($dir, 0777, true);
    }

    public static function createZip($files, $fName){
        $zip = new ZipArchive();
        if ($zip->open($fName, ZipArchive::CREATE) !== TRUE)
            throw new \Exception('Cannot create a zip file');

        foreach($files as $file){
            $f = basename($file);
            $zip->addFile($file, $f);
        }

        $zip->close();

    }

    public static function downloadFile($file, $fname){
        header("Content-Length: ".filesize($file));
        header("Content-Disposition: attachment; filename=".$fname);
        header("Content-Type: application/x-force-download; name=\"".$file."\"");
        readfile($file);
    }
}