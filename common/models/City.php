<?php

namespace common\models;

use common\models\lang\CityLang;
use yeesoft\behaviors\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property string $lat
 * @property string $lng
 *
 * @property Municipal[] $municipals
 * @property CityLang[] $forkLangs

 */
class City extends \yeesoft\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'langForeignKey' => 'city_id',
                'tableName' => "{{%city_lang}}",
                'attributes' => [
                    'name',
                ]
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    public static function map(){
        return ArrayHelper::map(City::find()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            [['lat', 'lng'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipals()
    {
        return $this->hasMany(Municipal::className(), ['city_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return CityQuery the active query used by this AR class.
     */
   public static function find()
   {
       return new CityQuery(get_called_class());
   }
}
