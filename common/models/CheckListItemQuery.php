<?php

namespace common\models;

use omgdef\multilingual\MultilingualTrait;

/**
 * This is the ActiveQuery class for [[CheckListItem]].
 *
 * @see CheckListItem
 */
class CheckListItemQuery extends \yii\db\ActiveQuery
{
    use MultilingualTrait;
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CheckListItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CheckListItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
