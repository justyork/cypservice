$(document).ready(function () {

    var _current_type = $('#current_type').val()
    var _current_service = $('#current_service').val()
    // SetType(_current_type)

    /**********************************************************/

    $('.service').on('click', '[data-type-set]', function () {
        var type_id = $(this).attr('data-type-set')
        setServiceType(_current_service, type_id)
        loadSteps(_current_service, type_id)
        // SetType(type_id)
    })

    /**********************************************************/

    $('.service').on('change', '.checklist-item', function () {
        var item_id = $(this).attr('data-id');
        var val = 0;
        if($(this).is(':checked')) val = 1;
        $.post('/api/save-check-list/', {id:item_id, val:val}, function(ret){
            if(ret.isNew){
                loadOrderList();
                $('.ajax-status-field').text(ret.formattedStatus);
            }
        });
    })

    /**********************************************************/

    $('.service').on('change', '.municipal-item', function () {
        var item_id = $(this).attr('data-id');
        var val = 0;
        if($(this).is(':checked')) val = 1;
        $.post('/api/save-municipal-list/', {id:item_id, val:val}, function(ret){
            if(ret.isNew){
                loadOrderList();
                $('.ajax-status-field').text(ret.formattedStatus);
            }
        });
    })

})

function loadOrderList(){
    $.post('/api/get-order-list/', {}, function(o){
        $('.ajax-order-field').html(o.data);
    });
}
function setServiceType (service, type_id) {
    $('#current_type').val(type_id)
    $.get('/api/set-service-type/?type_id=' + type_id);
}

function _loader () {
    return '<img src="/images/loader.svg" />'
}

function loadSteps (service, type) {
    $('.service').html(_loader())
    $.get('/api/steps-by-service-and-type/?service_id=' + service + '&type_id=' + type, function (o) {
        $('.service').html(o)
        SetType(type)
    })
}

function SetType (type_id) {
    'use strict'

    var active_class = 'btn-success'
    var default_class = 'btn-primary'
    var button_attr = 'data-type-set'

    var _current_type = parseInt(type_id)
    var types
    // $('.service-step').removeClass('hidden');
    // $('.service-step').each(function () {
    //   types = JSON.parse($(this).attr('data-step-type'));
    //   if(types.indexOf(_current_type) === -1)
    //     $(this).addClass('hidden');
    // });

    $('[' + button_attr + ']').removeClass(active_class).removeClass(default_class)
    $('[' + button_attr + ']').each(function () {
        if (_current_type == $(this).attr(button_attr))
            $(this).addClass(active_class)
        else
            $(this).addClass(default_class)
    })

}