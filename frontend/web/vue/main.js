var Vue = require('vue')
var VueResource = require('vue-resource')

var ServiceGet = require('./components/service/get.vue')

Vue.use(VueResource)

new Vue({
    el: '.vue-app',
    data: function () {
        return {}
    },
    components: {
        ServiceGet
    },
    methods: {},
    created: function () {
    },
})