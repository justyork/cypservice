import $ from 'jquery';
import magnificPopup from 'magnific-popup';

$('[data-modal]').magnificPopup({
    type: 'inline'
});