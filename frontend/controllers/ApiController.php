<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.01.2018
 * Time: 12:48
 */

namespace frontend\controllers;


use common\models\Order;
use common\models\OrderModule;
use common\models\Service;
use common\models\ServiceModule;
use yeesoft\helpers\Html;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ApiController extends \yeesoft\controllers\BaseController
{
    public $freeAccess = true;

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * Load steps by current step
     *
     * @param $service_id
     * @param $type_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionStepsByServiceAndType($service_id, $type_id)
    {
        /** @var \frontend\components\Service $service */
        $service = \Yii::$app->service;
        $model = $this->findService($service_id);
        $ret = '';
        foreach ($model->stepsByType($type_id) as $step)
            $ret .= $this->renderPartial('//service/_step', compact('step', 'service'));

        return $ret;
    }

    /**
     * Find service by id
     *
     * @param $id
     * @return null|Service
     * @throws NotFoundHttpException
     */
    private function findService($id)
    {
        $model = Service::findOne($id);
        if (!$model) throw new NotFoundHttpException();

        return $model;
    }

    /**
     * Set service current type
     *
     * @param $type_id
     */
    public function actionSetServiceType($type_id)
    {
        /** @var \frontend\components\Service $service */
        $service = \Yii::$app->service;
        $service->setType($type_id);
    }

    /**
     * Save check list item
     * @return array
     */
    public function actionSaveCheckList()
    {
        $item_id = $_POST['id'];
        $val = $_POST['val'];

        return $this->addModuleData(ServiceModule::MODULE_CHECK_LIST, $item_id, $val);
    }

    /**
     * Add module in user order
     *
     * @param $module
     * @param $item_id
     * @param $val
     * @return array
     */
    private function addModuleData($module, $item_id, $val)
    {
        $order = Order::findCurrentOrder();
        $model = $order->getOrderModules()->where(['module_name' => $module])->one();

        if (!$model) {
            $model = new OrderModule();
            $model->order_id = $order->id;
            $model->module_name = $module;
            $model->data = [];
        }

        $data = $model->data;
        $data[$item_id] = $val;
        $model->data = $data;

        if (!$model->save())
            return $model->errors;

        if ($order->isNewRecord)
            return [
                'isNew' => true,
                'orderName' => $order->number,
                'formattedStatus' => $order->formattedStatus
            ];

        return [];
    }

    /**
     * Save municipal data in user's order
     *
     * @return array
     */
    public function actionSaveMunicipalList()
    {
        $item_id = $_POST['id'];
        $val = $_POST['val'];

        return $this->addModuleData(ServiceModule::MODULE_MUNICIPAL, $item_id, $val);
    }

    public function actionGetOrderList(){
        /** @var \frontend\components\Service $service */
        $service = \Yii::$app->service;

        $list = Html::dropDownList('OrderList', $service->order->id, $service->orderList, [
            'prompt' => Yii::t('app', 'Create new'),
            'onchange' => "window.location = '".\yii\helpers\Url::to(['/service/set-order', 'id' => $service->id, 'order' => ''])."'+$(this).val();"
        ]);

        return ['data' => $list];
    }
}