<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:12
 */

namespace frontend\controllers;


use common\models\CMS;
use common\models\Order;
use common\models\Service;
use Mpdf\Mpdf;
use Yii;
use yii\base\Model;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

class ServiceController extends \yeesoft\controllers\BaseController
{

    public $freeAccess = true;

    /**
     * Get service
     *
     * @param $id
     * @param bool $form
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionGet($id, $form = false)
    {
        Yii::$app->user->setReturnUrl(['service/get', 'id' => $id, 'form' => $form]);
        $model = Service::findOne($id);
        if (!$model)
            throw new NotFoundHttpException('Page not found.');

        if ($form) return $this->getForm($model, $form);

        return $this->render('get', compact('model'));
    }


    /**
     * Get form for filling by user
     *
     * @param Service $service
     * @param $form_name
     * @return string
     */
    private function getForm(Service $service, $form_name)
    {
        $model_name = 'common\models\forms\\' . CMS::IdToClass($form_name);

        /**
         * @var $model Model
         */
        $model = new $model_name();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Data successfully saved'));
            return $this->refresh();
        }
        return $this->render('form', compact('model', 'service', 'form_name'));
    }

    /**
     * Generate test document
     *
     * @param $service_id
     * @throws NotFoundHttpException
     * @throws UnauthorizedHttpException
     * @throws \Mpdf\MpdfException
     */
    public function actionDocument($service_id)
    {
        if (Yii::$app->user->isGuest)
            throw new UnauthorizedHttpException(Yii::t('app', 'You are not authorized to perform this action'));

        $model = Service::findOne($service_id);
        if (!$model)
            throw new NotFoundHttpException(Yii::t('app', 'Page not found'));

        $this->layout = false;
        $isPaid = false;

        $content = $this->render('document', compact('model', 'isPaid'));

        $pdf = new Mpdf();
        $stylesheet = file_get_contents(Yii::getAlias('@frontend/web/css/form_pdf.css'));
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($content, 2);

        $pdf->Output();
    }

    /**
     * @param $price
     * @param $id
     * @return \yii\web\Response
     */
    public function actionPay($price, $id)
    {
        /** @var \frontend\components\Service $service */
        $service = Yii::$app->service;
        $service->setService($id);
        $service->pay($price);
        Yii::$app->session->setFlash('success', Yii::t('app', 'You successfully paid {price}', ['price' => CMS::FormatPrice($price)]));

        return $this->goBack();
    }

    /** Close order and get files
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionClose($id){
        /** @var \frontend\components\Service $service */
        $service = Yii::$app->service;
        $service->setService($id);
        $service->getCollectData();
        return $this->goBack();
    }

    /**
     * Get service model
     *
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    private function findService($id){
        $model = Service::findOne($id);
        if(!$model) throw new NotFoundHttpException();
        return $model;
    }

    /** Download generated document
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionDownloadDoc(){
        /** @var \frontend\components\Service $service */
        $service = Yii::$app->service;

        if(!$service || $service->order->status != Order::STATUS_COMPLETE)
            throw new BadRequestHttpException(Yii::t('app', 'Bad request'));

        CMS::downloadFile($service->documentPath, $service->documentName);
        return $this->goBack();
    }

    public function actionSetOrder($id, $order = ''){
        /** @var \frontend\components\Service $service */
        $service = Yii::$app->service;
        $service->setService($id);
        $service->setOrder($order);
        return $this->goBack();
    }

    public function actionSend(){
        /** @var \frontend\components\Service $service */
        $service = Yii::$app->service;
        $service->order->status = Order::STATUS_MODERATE;
        $service->order->save();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Your order was sent to moderation'));

        return $this->goBack();
    }
}