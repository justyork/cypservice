<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 15.01.2018
 * Time: 19:41
 *
 * @var $model \common\models\Municipal[]
 */


$pdf = new \frontend\models\PDF();
?>
<div class="content">

    <h1><?=Yii::t('app', 'Municipal services');?></h1>
    <table>
        <?foreach($model as $item):
            $center = "{$item->lat},{$item->lng}";
        ?>
            <tr>
                <td style="padding-bottom: 20px"><img src="https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=300x300&maptype=roadmap&center=<?= $center?>&markers=color:green|<?= $center?>" alt=""></td>
                <td style="vertical-align: top; padding-left: 10px">
                    <?= $pdf->lst(Yii::t('app', 'City'), $item->city->name)?>
                    <?= $pdf->lst(Yii::t('app', 'Address'), $item->address)?>
                    <?= $pdf->lst(Yii::t('app', 'Phone'), $item->phone)?>
                    <?if($item->email):?>
                        <?= $pdf->lst(Yii::t('app', 'Email'), $item->email)?>
                    <?endif?>
                    <?= $pdf->lst(Yii::t('app', 'Website'), $item->website)?>
                    <?= $pdf->lst(Yii::t('app', 'Work Time'), $item->work_time)?>
                </td>
            </tr>
        <?endforeach?>
    </table>
</div>

