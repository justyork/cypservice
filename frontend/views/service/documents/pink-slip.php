<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 08.12.2017
 * Time: 15:59
 *
 * @var $service \common\models\Service
 * @var $isPaid bool
 */

$check_lists = [
    [
        'header' => 'ΕΙΔΟΣ ΑΙΤΗΣΗΣ/TYPE OF APPLICATION ',
        'header_small' => '*Σημειώστε Χ ή √ στο σχετικό τετραγωνάκι/Mark X or √ in the appropriate box:',
        'inline' => false,
        'items' => [
            [
                'title' => 'Έκδοση Άδειας Προσωρινής Διαμονής και Εγγραφή',
                'small' => 'Issue of a Temporary Residence Permit and Registration',
            ],
            [
                'title' => 'Έκδοση Άδειας Προσωρινής Διαμονής ',
                'small' => 'Issue of a Temporary Residence Permit',
            ],
            [
                'title' => 'Ανανέωση Άδειας Προσωρινής Διαμονής',
                'small' => 'Renewal of a Temporary Residence Permit',
            ],
        ]
    ],
    [
        'header' => 'ΣΚΟΠΟΣ ΠΡΟΣΩΡΙΝΗΣ ΑΔΕΙΑΣ ΔΙΑΜΟΝΗΣ/PURPOSE OF TEMPORARY RESIDENCE PERMIT',
        'header_small' => '*Σημειώστε Χ ή √ στο σχετικό τετραγωνάκι/Mark X or √ in the appropriate box:',
        'inline' => true,
        'items' => [
            [
                'title' => 'Επισκέπτης',
                'small' => 'Visitor',
            ],
            [
                'title' => 'Επισκέπτης – Εξαρτώμενος Ανήλικος (εξαιρουμένων των μαθητών)',
                'small' => 'Visitor – Dependent Minor (excluding pupils)',
            ],
            [
                'title' => 'Επισκέπτης – Εξαρτώμενος μαθητής',
                'small' => 'Visitor – Dependent pupil',
            ],
            [
                'title' => 'Επισκέπτης με σκοπό την τέλεση γάμου με Κύπριο ή Ευρωπαίο πολίτη',
                'small' => 'Visitor with the purpose of performing a marriage with a Cypriot or a European Citizen',
            ],
            [
                'title' => 'Επισκέπτης Μέλος Οικογένειας Εργαζομένου σε Εταιρεία Διεθνών Δραστηριοτήτων',
                'small' => 'Visitor Family Member of an Employee of a Foreign Company.',
            ],
            [
                'title' => 'Επισκέπτης – Εξαρτώμενος Ανήλικος (εξαιρουμένων των μαθητών) Μέλος Οικογένειας Εργαζομένου σε Εταιρεία Διεθνών Δραστηριοτήτων',
                'small' => 'Visitor – Dependent Minor (excluding pupils) Family Member of an Employee of a Foreign Company.',
            ],
            [
                'title' => 'Επισκέπτης – Εξαρτώμενος μαθητής Μέλος Οικογένειας Εργαζομένου σε Εταιρεία Διεθνών Δραστηριοτήτων',
                'small' => 'Visitor – Dependent pupil Family Member of an Employee of a Foreign Company.',
            ],
        ]
    ],
];

$model = new \common\models\forms\PinkSlip();
$pdf = new \frontend\models\PDF();
//$pdf->debug = true;

$tpl_class = $isPaid ? '' : 'template'; 

?>
<section style="background: url(/images/pink_slip/page0001.jpg)">
    <div class="<?= $tpl_class?>">&nbsp;</div>
</section>
<section style="background: url(/images/pink_slip/page0002.jpg)" class="<?= $pdf->debug ? 'debug' : ''?>">

    <div class="<?= $tpl_class?>">
    <?= $pdf->clear()?>
    <?= $pdf->blockStart(425, 150)?>
        <?= $pdf->v($model->surname, 113, 50, 360) ?>
        <?= $pdf->v($model->middle_name, 15, 50, 360) ?>
        <?= $pdf->v($model->date_birth . ' ' . $model->place_birth, 18, 50, 360) ?>

    <?= $pdf->blockEnd()?>


    <?= $pdf->blockStart(365, 150)?>
        <?= $pdf->v($model->name, 113, 9, 330) ?>
        <?= $pdf->v($model->nationality, 15, 9, 330) ?>
        <?= $pdf->v($model->country, 18, 9, 330) ?>
    <?= $pdf->blockEnd()?>

    <?= $pdf->clear()?>

    <?= $pdf->v($model->marital_status == 1 ? 'X' : ' ', 23, 92, 30) ?>
    <?= $pdf->v($model->marital_status == 2 ? 'X' : ' ', 0, 65, 30) ?>
    <?= $pdf->v($model->marital_status == 3 ? 'X' : ' ', 0, 65, 30) ?>
    <?= $pdf->v($model->marital_status == 4 ? 'X' : ' ', 0, 75, 30) ?>
    <?= $pdf->v($model->marital_status == 5 ? 'X' : ' ', 0, 56, 30) ?>
    <?= $pdf->clear()?>

    <?= $pdf->v($model->number_of_marriage, 22, 669, 60) ?>

    <?= $pdf->clear(15)?>

    <?= $pdf->blockStart(425, 135)?>
        <?= $pdf->v($model->travel_doc_no, 37, 100)?>
        <?= $pdf->v($model->travel_doc_place, 5, 220)?>
        <?= $pdf->v($model->travel_doc_expire, 5, 204)?>

    <?= $pdf->blockEnd()?>
    <?= $pdf->blockStart(365, 135)?>
        <?= $pdf->v($model->port_entry, 30, 175)?>
        <?= $pdf->v($model->date_entry, 3, 165)?>

        <?= $pdf->v($model->gender == 1 ? 'X' : ' ', 33, 110, 30) ?>
        <?= $pdf->v($model->gender == 2 ? 'X' : ' ', 0, 80, 30) ?>
        <?= $pdf->v($model->gender == 3 ? 'X' : ' ', 0, 70, 30) ?>
    <?= $pdf->blockEnd()?>
    <?= $pdf->blockStart(445, 160)?>
        <?= $pdf->v($model->residence_arc, 60, 120)?>
        <?= $pdf->v($model->residence_status, 4, 265, 170)?>
        <?= $pdf->v($model->residence_date_issue, 4, 210)?>
        <?= $pdf->v($model->residence_date_expire, 4, 204)?>

    <?= $pdf->blockEnd()?>
    <?= $pdf->blockStart(345, 160)?>
        <?= $pdf->v($model->home_phone, 35, 195)?>
        <?= $pdf->v($model->office_phone, 4, 213, 165)?>
        <?= $pdf->v($model->mobile_phone, 4, 90)?>
        <?= $pdf->v($model->fax, 4, 170)?>
        <?= $pdf->v($model->email, 4, 98)?>
    <?= $pdf->blockEnd()?>

    <?= $pdf->blockStart(445, 120)?>


        <?= $pdf->v($model->address_street, 43, 50, 390)?>
        <?= $pdf->v($model->address_area, 0, 140)?>

        <?= $pdf->clear()?>

        <?= $pdf->v($model->address_city, 6, 110, 80)?>
        <?= $pdf->v($model->address_postal, 0, 180, 60)?>

    <?= $pdf->blockEnd()?>
    <?= $pdf->blockStart(345, 120)?>
        <?= $pdf->v($model->abroad_street, 43, 5, 320)?>
        <?= $pdf->v($model->abroad_city, 0, 70, 250)?>
        <?= $pdf->v($model->abroad_country, 10, 90, 230)?>
    <?= $pdf->blockEnd()?>

    <?= $pdf->v($model->cor_address_street, 29, 265, 480)?>
    <?= $pdf->v($model->cor_address_area, 3, 130)?>
    <?= $pdf->v($model->cor_address_city, 0, 80, 100)?>
    <?= $pdf->v($model->cor_address_postal, 0, 150, 60)?>

    <?= $pdf->clear(45)?>

    <?= $pdf->v($model->residence_own_house == 1 ? 'X' : '', 0, 359, 60)?>
    <?= $pdf->v($model->residence_rented_house == 1 ? 'X' : '', 0, 322, 30)?>

    <?= $pdf->clear()?>

    <?= $pdf->v($model->residence_other, 6, 345, 350)?>
    <?= $pdf->v($model->residence_other != '' ? 'X' : '', 5, 45, 30)?>

    <?= $pdf->clear()?>
    </div>

</section>
<section style="background: url(/images/pink_slip/page0003.jpg)" class="<?= $pdf->debug ? 'debug' : ''?>">
    <div class="<?= $tpl_class?>">&nbsp;
        <?= $pdf->clear(180)?>
        
        <?= $pdf->blockStart(175, 140)?>
            <?foreach($model->family_name as $val):?>
                <?= $pdf->v($val, 0, 50, 130, 27)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(88, 140)?>
            <?foreach($model->family_nationality as $val):?>
                <?= $pdf->v($val, 0, 5, 80, 27)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(98, 140)?>
            <?foreach($model->family_date_birth as $key => $val):?>
                <?= $pdf->v($val, 0, 5, 90, 12)?>
                <?= $pdf->clear()?>
                <?= $pdf->v($model->family_place_birth[$key], 0, 5, 90, 14)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(80, 140)?>
            <?foreach($model->family_relation as $val):?>
                <?= $pdf->v($val, 0, 2, 78, 27)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(50, 140)?>
            <?foreach($model->family_sex as $key => $val):?>
                <?= $pdf->v($model->family_name[$key] ? \common\models\forms\PinkSlip::getGenderList()[$val] : '', 0, 5, 45, 27)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(78, 140)?>
            <?foreach($model->family_country as $val):?>
                <?= $pdf->v($val, 0, 5, 72, 27)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(65, 140)?>
            <?foreach($model->family_arc as $val):?>
                <?= $pdf->v($val, 0, 2, 63, 27)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
        <?= $pdf->blockStart(150, 140)?>
            <?foreach($model->family_document as $key => $val):?>
                <?= $pdf->v($val, 0, 5, 130, 12)?>
                <?= $pdf->clear()?>
                <?= $pdf->v($model->family_document_expire[$key], 0, 5, 130, 14)?>
                <?= $pdf->clear()?>
            <?endforeach?>
        <?= $pdf->blockEnd()?>
    </div>
</section>
<section style="background: url(/images/pink_slip/page0004.jpg)">
    <div class="<?= $tpl_class?>">&nbsp;</div>
</section>
<section style="background: url(/images/pink_slip/page0005.jpg)">
    <div class="<?= $tpl_class?>">&nbsp;</div>
</section>
<section style="background: url(/images/pink_slip/page0006.jpg)">
    <div class="<?= $tpl_class?>">&nbsp;</div>
</section>
<section style="background: url(/images/pink_slip/page0007.jpg)">
    <div class="<?= $tpl_class?>">&nbsp;</div>
</section>