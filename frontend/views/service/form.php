<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 21.11.2017
 * Time: 12:46
 *
 * @var $this \yii\web\View
 * @var $model \common\models\FormModel
 * @var $form_name string
 * @var $service \common\models\Service
 */

$this->params['breadcrumbs'] = [
    ['label' => $service->name, 'url' => ['service/get', 'id' => $service->id]] ,
    Yii::t('app', 'Form')
];

?>

<?= \yii\helpers\Html::a('<i class="ion-ios-undo-outline"></i>'.Yii::t('app', 'Go back'), ['service/get', 'id' => $service->id], ['class' => 'btn margin-bottom'])?>
<?= $this->render('forms/'.$form_name, compact('model', 'service', 'form_name'))?>