<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */

if(Yii::$app->user->isGuest)
    echo \common\models\CMS::NeedAuthAlert();
elseif($service->isCompleted)
    echo \common\models\CMS::OrderCompleted();
elseif($service->isActive)
    echo \yii\helpers\Html::a(Yii::t('app', 'Chek documents'), ['service/document', 'service_id' => $step->service_id, 'order_id' => $service->order_id], ['target' => '_blank', 'class' => 'btn btn-default']);

