<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */


use common\models\CMS;

$price = $step->module_id;
$paid = $service->paid;

if($price > $service->paid)
    $price = $price - $service->paid;

?>

<?if(Yii::$app->user->isGuest):?>
    <?= \common\models\CMS::NeedAuthAlert()?>
<?else:?>
    <div class="pay-step">

        <? if($service->paid >= $step->module_id):?>
            <div class="pay-step__price">
                <span disabled href="javascript:;"> <i class="ion-ios-checkmark"></i> <?= Yii::t('app', 'Service already paid')?></span>
            </div>
        <?else:?>
            <div class="pay-step__price">
                <?if($paid):?>
                    <?= Yii::t('app', 'Cost: {price} ({paid} already paid)', ['price' => CMS::FormatPrice($price), 'paid' => CMS::FormatPrice($paid)])?>
                <?else:?>
                    <?= Yii::t('app', 'Cost: {price}', ['price' => CMS::FormatPrice($price)])?>
                <?endif?>
            </div>
            <div class="pay-step__button">
                <a href="<?= \yii\helpers\Url::to(['service/pay', 'id' => $step->service_id, 'price' => $price])?>">
                    <img src="https://woocommerce.com/wp-content/uploads/2016/04/PayPal-Express@2x.png" style="width: 200px" />
                </a>
            </div>
        <?endif?>


    </div>

<?endif?>
