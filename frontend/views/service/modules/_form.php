<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */

$form = $step->module_id;

if(Yii::$app->user->isGuest)
    echo \common\models\CMS::NeedAuthAlert();
elseif($service->isCompleted)
    echo \common\models\CMS::OrderCompleted();
else
    echo \yii\helpers\Html::a(Yii::t('app', 'Fill the form'), ['service/get', 'id' => $step->service_id, 'form' => $form ], ['class' => 'btn btn-success']);
