<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 14.01.2018
 * Time: 11:17
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */

// Is authorised
if(Yii::$app->user->isGuest) echo \common\models\CMS::NeedAuthAlert();
// Is order completed
elseif($service->isCompleted)
    echo \common\models\CMS::OrderCompleted(). ' '. \yii\helpers\Html::a($service->documentName, ['service/download-doc']);
// Is not paid for service
elseif(!$service->isPaid) echo \common\models\CMS::NeedToPay();
// Is active
elseif($service->isActive)
    echo \yii\helpers\Html::a(Yii::t('app', 'Close order'), ['service/close', 'id' => $service->id], ['class' => 'btn btn-success']);



