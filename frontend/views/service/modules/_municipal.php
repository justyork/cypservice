<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */

$model = \common\models\Municipal::find()->where(['id' => $step->module_id])->all();


$checked_items = [];
$order = $service->order;
if($order)
    $checked_items = $order->getOrderModules()->where(['module_name' => \common\models\ServiceModule::MODULE_MUNICIPAL])->one();
?>
<table class="table">
    <thead>
        <tr>
            <?if(!Yii::$app->user->isGuest && $service->isActive):?>
                <th>

                </th>
            <?endif;?>
            <th><?=Yii::t('app', 'Address');?> </th>
            <th><?=Yii::t('app', 'Phone');?> </th>
            <th><?=Yii::t('app', 'Work time');?> </th>
            <th><?=Yii::t('app', 'Website');?> </th>
        </tr>
    </thead>
    <tbody>
        <?foreach ($model as $item):?>
            <tr>
                <?if(!Yii::$app->user->isGuest && $service->isActive):?>
                    <td>
                        <?=\yii\helpers\Html::checkbox('municipal-item[]',
                            isset($checked_items->data[$item->id]) && $checked_items->data[$item->id] == 1, [
                                'id' => 'municipal-cb-'.$item->id,
                                'class' => 'municipal-item',
                                'data-id' => $item->id
                            ])?>
                    </td>
                <?endif;?>
                <td><label for="municipal-cb-<?= $item->id?>"><?=$item->address?></label></td>
                <td><a href="tel:<?= $item->phone ?>"><?=$item->phone?></a></td>
                <td><?=$item->work_time?></td>
                <td><a target="_blank" href="http://<?= $item->website ?>"><?=$item->website?></a></td>
            </tr>

        <?endforeach;?>
    </tbody>
</table>

