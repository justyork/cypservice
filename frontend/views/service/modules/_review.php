<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */

use yii\widgets\ActiveForm;

if(Yii::$app->user->isGuest)
    echo \common\models\CMS::NeedAuthAlert();
elseif($service->isPaid && $service->isActive)
    echo \yii\helpers\Html::a(Yii::t('app', 'Send'), ['service/send']);
elseif(!$service->isPaid)
    echo \common\models\CMS::NeedToPay();

?>
<?if(!$service->isCompleted):?>
    <?php $form = ActiveForm::begin([
        'id' => '',
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>
        <?foreach($step->module_id as $item):?>
            <div class="form-group">
                <label for=""><?= \common\models\Documents::map()[$item]?></label>
                <?= \yii\helpers\Html::fileInput('Documents['.\common\models\Documents::map()[$item].']')?>
            </div>
        <?endforeach?>
    <?php ActiveForm::end(); ?>
<?endif?>


