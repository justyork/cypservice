<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 */

$service = \common\models\Service::findOne($step->module_id);

?>

<div>
    <?= \yii\helpers\Html::a($service->name, ['service/get', 'id' => $service->id], ['class' => 'btn btn-success', 'target' => '_blank'])?>
</div>
