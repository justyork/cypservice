<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */
$forks = \common\models\Fork::find()->where(['id' => $step->module_id])->all();
?>
<div>
    <div class="btn-group" role="group">
        <?foreach ($forks as $item):?>
            <button type="button" class="btn <?= $service->type == $item->id ? 'btn-success' : 'btn-success-border'?>" <?= $service->isCompleted ? 'disabled' : ''?> data-type-set="<?=$item->id?>"><?=$item->name?></button>
        <?endforeach;?>
    </div>
</div>

