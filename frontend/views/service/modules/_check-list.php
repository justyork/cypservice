<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */

$model = \common\models\CheckList::findOne($step->module_id);

$checked_items = [];
$order = $service->order;
if($order)
    $checked_items = $order->getOrderModules()->where(['module_name' => \common\models\ServiceModule::MODULE_CHECK_LIST])->one();

?>

<table class="table">
    <tbody>
        <?foreach ($model->checkListItems as $item):?>
            <tr>
                <?if(!Yii::$app->user->isGuest && $service->isActive):?>
                    <td>
                        <?=\yii\helpers\Html::checkbox('checklist-item[]',
                            isset($checked_items->data[$item->id]) && $checked_items->data[$item->id] == 1, [
                                'id' => 'checklist-cb-'.$item->id,
                                'class' => 'checklist-item',
                                'data-id' => $item->id
                            ])?>
                    </td>
                <?endif;?>
                <td>
                    <label for="checklist-cb-<?=$item->id?>"><?=$item->value?></label>
                </td>
            </tr>
        <?endforeach;?>
    </tbody>
</table>
