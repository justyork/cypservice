<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 */

?>
<?if(Yii::$app->user->isGuest):?>
    <a href="<?=\yii\helpers\Url::to(['auth/login'])?>" class="btn btn-success margin-right-small">Login</a> <?=Yii::t('app', 'or');?>  <a href="<?= \yii\helpers\Url::to(['site/registration'])?>">Registration</a>
<?else:?>
    <div class="alert alert-success"><?=Yii::t('app', 'You successfully logged in');?> </div>
<?endif;?>
