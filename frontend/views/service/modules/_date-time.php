<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 20.12.2017
 * Time: 16:51
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 */
$y = (int)date('Y');
$years = [
    $y => $y, $y+1
];

?>

<div>

    <?= \yii\helpers\Html::dropDownList('day', '', \common\models\CMS::DayList())?>
    <?= \yii\helpers\Html::dropDownList('month', '', \common\models\CMS::MonthList())?>
    <?= \yii\helpers\Html::dropDownList('year', '', $years)?>

    <?= \yii\helpers\Html::dropDownList('hour', '', \common\models\CMS::HourList())?>
    <?= \yii\helpers\Html::dropDownList('minutes', '', \common\models\CMS::MinuteList())?>

    <?if(!Yii::$app->user->isGuest):?>
        <?= \yii\helpers\Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    <?endif;?>
</div>

