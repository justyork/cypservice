<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:44
 *
 * @var $this \yii\web\View
 * @var $step \common\models\ServiceStep
 * @var $service \frontend\components\Service
 */
$moduleIcon = [
    'fork' => 'ion-ios-shuffle',
    'check-list' => 'ion-ios-list-outline',
    'login' => 'ion-ios-personadd-outline',
    'download' => 'ion-ios-cloud-download-outline',
    'form' => 'ion-ios-compose-outline',
    'municipal' => 'ion-ios-location-outline',
    'advice-service' => 'ion-ios-pulse',
    'review' => 'ion-ios-cloud-upload-outline',
    'get-document' => 'ion-ios-glasses-outline',
    'pay' => 'ion-ios-cart-outline',
    'close' => 'ion-ios-checkmark-outline',
];
?>


<div class="service-step" data-step-type="<?=$step->type?>">
    <div class="service-step__left">
        <?if(empty($step->module_name)):?>
            <i class="ion-ios-paper-outline"></i>
        <?else:?>
            <i class="<?= $moduleIcon[$step->module_name]?>"></i>
        <?endif?>
    </div>
    <div class="service-step__content">
        <?if(!empty($step->title)):?><h4><?=$step->title?></h4><?endif?>
        <?if(!empty($step->text)):?><p class="margin-bottom"><?=$step->text?></p><?endif?>
        <?if(!empty($step->module_name)):?>
            <?=$this->render('modules/_'.$step->module_name, compact('step', 'service'))?>
            <div class="clearfix"></div>
        <?endif;?>
        <p><?=$step->text_bottom?></p>
    </div>
</div>
