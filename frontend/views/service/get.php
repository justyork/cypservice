<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 20:22
 *
 * @var $this \yii\web\View
 * @var $model \common\models\Service
 *
 */

use yii\helpers\Html;

/** @var \frontend\components\Service $service */
$service = Yii::$app->service;

$this->params['breadcrumbs'] = [
    $model->name
];

$service->setService($model->id);

if(!$service->type) $service->type = 1;

$this->registerJsFile('/js/service.js', ['depends' => 'yii\web\YiiAsset']);
?>

<h1><?=$model->name?></h1>
<div class="row margin-bottom">
    <div class="col-sm-6">
        <table class="table">
            <tr>
                <td><?=Yii::t('app', 'Price');?></td>
                <td><?= $model->cost?></td>
            </tr>
            <tr>
                <td><?=Yii::t('app', 'Period');?></td>
                <td><?= $model->time?></td>
            </tr>
            <?if(!Yii::$app->user->isGuest):?>
                <tr>
                    <td><b><?=Yii::t('app', 'Order');?> </b></td>
                    <td class="ajax-order-field">
                        <?= Html::dropDownList('OrderList', $service->order->id, $service->orderList, [
                            'prompt' => Yii::t('app', 'Create new'),
                            'onchange' => "
                                window.location = '".\yii\helpers\Url::to(['/service/set-order', 'id' => $service->id, 'order' => ''])."'+$(this).val();
                            "
                        ])?>
                    </td>
                </tr>
                <tr>
                    <td><b><?=Yii::t('app', 'Status');?> </b></td>
                    <td class="ajax-status-field"><?= $service->order ? $service->order->formattedStatus : Yii::t('app', '-- none --')?></td>
                </tr>
                <tr>
                    <td><b><?=Yii::t('app', 'Paid');?> </b></td>
                    <td><?= $service->paid ? $service->paid : Yii::t('app', '-- none --')?></td>
                </tr>
            <?endif?>
        </table>
    </div>
</div>
<?=Html::hiddenInput('current_type', $service->type, ['id' => 'current_type']);?>
<?=Html::hiddenInput('current_order', $service->order_id, ['id' => 'current_order']);?>
<?=Html::hiddenInput('current_service', $service->id, ['id' => 'current_service']);?>
<div class="service">
    <?foreach ($model->stepsByType($service->type) as $step):?>
        <?=$this->render('_step', compact('step', 'service'))?>
    <?endforeach;?>
</div>