<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 22.11.2017
 * Time: 15:08
 */

use common\models\forms\PinkSlip;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View
 * @var $model \common\models\forms\PinkSlip
 * @var $service \common\models\ServiceStep
 */

?>


<?php $form = ActiveForm::begin([
    'id' => 'pink-slip'
]); ?>

    <h1><?= Yii::t('app', 'Pink slip form'); ?> </h1>

    <div><?=$form->errorSummary($model)?></div>

    <h3><?= Yii::t('app', 'PART I – PARTICULARS OF THE ALIEN'); ?> </h3>
    <div class="row">
        <div class="col-sm-6"><?= $form->field($model, 'name')->textInput() ?></div>
        <div class="col-sm-6"><?= $form->field($model, 'surname')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-sm-6"><?= $form->field($model, 'middle_name')->textInput() ?></div>
        <div class="col-sm-6"><?= $form->field($model, 'nationality')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'date_birth')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'place_birth')->textInput() ?>
        </div>
        <div class="col-sm-6"><?= $form->field($model, 'country')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'marital_status')->dropDownList(PinkSlip::getMaritalStatusList()) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'number_of_marriage')->textInput() ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <h4><?= Yii::t('app', 'Particulars of the Travel Document'); ?></h4>
            <?= $form->field($model, 'travel_doc_no')->textInput() ?>
            <?= $form->field($model, 'travel_doc_place')->textInput() ?>
            <?= $form->field($model, 'travel_doc_expire')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <h4><?= Yii::t('app', 'Particulars of the last entry in the Republic'); ?> </h4>
            <?= $form->field($model, 'port_entry')->textInput() ?>
            <?= $form->field($model, 'date_entry')->textInput() ?>

            <?=$form->field($model, 'gender')->dropDownList(PinkSlip::getGenderList())?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <h4><?= Yii::t('app', 'Particulars of residence permit in the Republic'); ?> </h4>
            <p><?= Yii::t('app', 'Fill-in in case the alien is/was a holder of a residence permit in the Republic'); ?></p>
            <?= $form->field($model, 'residence_arc')->textInput() ?>
            <?= $form->field($model, 'residence_status')->textInput() ?>
            <?= $form->field($model, 'residence_date_issue')->textInput() ?>
            <?= $form->field($model, 'residence_date_expire')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <h4><?= Yii::t('app', 'Contact details in the Republic'); ?> </h4>
            <?= $form->field($model, 'home_phone')->textInput() ?>
            <?= $form->field($model, 'office_phone')->textInput() ?>
            <?= $form->field($model, 'mobile_phone')->textInput() ?>
            <?= $form->field($model, 'fax')->textInput() ?>
            <?= $form->field($model, 'email')->textInput() ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <h4><?= Yii::t('app', 'Residence address in the Republic'); ?> </h4>
            <?= $form->field($model, 'address_street')->textInput() ?>
            <?= $form->field($model, 'address_area')->textInput() ?>
            <?= $form->field($model, 'address_city')->textInput() ?>
            <?= $form->field($model, 'address_postal')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <h4><?= Yii::t('app', 'Residence address abroad'); ?> </h4>
            <?= $form->field($model, 'abroad_street')->textInput() ?>
            <?= $form->field($model, 'abroad_city')->textInput() ?>
            <?= $form->field($model, 'abroad_country')->textInput() ?>
        </div>
    </div>
    <hr>
    <h4><?= Yii::t('app', 'Correspondence address in the Republic'); ?> </h4>
    <?= $form->field($model, 'cor_different')->checkbox([
        'onchange' => "
        var active = $(this).is(':checked');
        if(active) $('#cor_data').show();
        else $('#cor_data').hide();
        "
    ])?>
    <div class="row" id="cor_data" style="display: none;">
        <div class="col-sm-12">
            <p><?= Yii::t('app', 'Fill-in in case it is different from the residence address'); ?> </p>
            <?= $form->field($model, 'cor_address_street')->textInput() ?>
        </div>
        <div class="col-sm-4"><?= $form->field($model, 'cor_address_area')->textInput() ?></div>
        <div class="col-sm-4"><?= $form->field($model, 'cor_address_city')->textInput() ?></div>
        <div class="col-sm-4"><?= $form->field($model, 'cor_address_postal')->textInput() ?></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12">
            <h4><?= Yii::t('app', 'State the kind of accommodation where you reside in the Republic'); ?></h4>
        </div>
        <div class="col-sm-3"><?= $form->field($model, 'residence_own_house')->checkbox() ?></div>
        <div class="col-sm-3"><?= $form->field($model, 'residence_rented_house')->checkbox() ?></div>
        <div class="col-sm-6"><?= $form->field($model, 'residence_other')->textInput() ?></div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <h3><?=Yii::t('app', 'Income');?></h3>
            <table class="table">
                <thead>
                    <tr>
                        <th><?=Yii::t('app', 'Interests from deposits, shares, rents (State the bank, company and the type of property)');?> </th>
                        <th><?=Yii::t('app', 'Country');?> </th>
                        <th><?=Yii::t('app', 'Amount (€)');?> </th>
                    </tr>
                </thead>
                <?for($i = 0; $i < 4; $i++):?>
                    <tr>
                        <td><?=Html::activeTextInput($model, 'income_name[]', ['class' => 'form-control form-control-sm'])?></td>
                        <td><?=Html::activeTextInput($model, 'income_country[]', ['class' => 'form-control form-control-sm'])?></td>
                        <td><?=Html::activeTextInput($model, 'income_amount[]', ['class' => 'form-control form-control-sm'])?></td>
                    </tr>
                <?endfor;?>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h3><?=Yii::t('app', 'PART II – PARTICULARS OF THIRD COUNTRY NATIONAL’S FAMILY MEMBERS (SPOUSE AND CHILDREN)');?></h3>
        </div>

        <?for($i = 0; $i < 4; $i++):?>
            <div class="col-sm-6">
                <div class="card card-border">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6"><?=$form->field($model, 'family_name['.$i.']')->textInput()?></div>
                            <div class="col-sm-6"><?=$form->field($model, 'family_nationality['.$i.']')->textInput()?></div>
                            <div class="col-sm-3"><?=$form->field($model, 'family_date_birth['.$i.']')->textInput()?></div>
                            <div class="col-sm-3"><?=$form->field($model, 'family_place_birth['.$i.']')->textInput()?></div>
                            <div class="col-sm-6"><?=$form->field($model, 'family_relation['.$i.']')->textInput()?></div>
                            <div class="col-sm-6"><?=$form->field($model, 'family_document['.$i.']')->textInput()?></div>
                            <div class="col-sm-6"><?=$form->field($model, 'family_document_expire['.$i.']')->textInput()?></div>
                            <div class="col-sm-3"><?=$form->field($model, 'family_sex['.$i.']')->dropDownList(PinkSlip::getGenderList())?></div>
                            <div class="col-sm-5"><?=$form->field($model, 'family_country['.$i.']')->textInput()?></div>
                            <div class="col-sm-4"><?=$form->field($model, 'family_arc['.$i.']')->textInput()?></div>
                        </div>

                    </div>
                </div>
            </div>
        <?endfor;?>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success"><?=Yii::t('app', 'Save');?> </button>
        </div>
    </div>

<?php ActiveForm::end(); ?>