<?php
/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use frontend\assets\AppAsset;
use frontend\assets\ThemeAsset;
use yeesoft\models\Menu;
use yeesoft\widgets\LanguageSelector;
use yeesoft\widgets\Nav as Navigation;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yeesoft\comment\widgets\RecentComments;

Yii::$app->assetManager->forceCopy = true;
AppAsset::register($this);
//ThemeAsset::register($this);

$headerAuth = [];
if(Yii::$app->user->isGuest){
    $headerAuth[] = ['label' => Yii::t('yee/auth', 'Login'), 'url' => ['/auth/default/login']];
    $headerAuth[] = ['label' => Yii::t('yee/auth', 'Signup'), 'url' => ['/auth/default/signup']];
}
else{
    $headerAuth[] = ['label' => Yii::$app->user->identity->username,'url' => ['/auth/default/profile']];
    $headerAuth[] = ['label' => Yii::t('yee/auth', 'Logout'), 'url' => ['/auth/default/logout', 'language' => false], 'template' => '<a href="{url}" data-method="post">{label}</a>'];
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <?= $this->renderMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="header">
    <a class="header-logo">CYP SERVICE</a>
    <nav class="header-langs">
        <?= LanguageSelector::widget(['display' => 'label']);?>
    </nav>
    <nav class="header-nav">
        <?= \yii\widgets\Menu::widget([
            'items' => $headerAuth
        ])?>
    </nav>
    <nav class="header-nav">
        <?= \yii\widgets\Menu::widget([
            'items' => Menu::getMenuItems('main-menu')
        ])?>
    </nav>
</div>

<section class="content vue-app">
    <?if(!\common\models\CMS::isHomePage()):?>
        <div class="container-box">
    <?endif?>
    <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 'options' => ['class' => 'breadcrumbs']]) ?>
    <?= Alert::widget() ?>
    <?= $content?>
    <?if(!\common\models\CMS::isHomePage()):?>
        </div>
    <?endif?>
</section>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->settings->get('general.title', 'Yee Site', Yii::$app->language)) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?>, <?= yeesoft\Yee::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
