<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 23.10.2017
 * Time: 19:48
 */
/* @var $this \yii\web\View
 * @var $service \common\models\Service
 *
 */


?>

<div class="col-sm-6 col-md-3 col-xs-12">
    <a href="<?=$service->link?>" class="popular-service">
        <div class="popular-service__icon">
            <i class="<?=$service->icon?>"></i>
        </div>
        <div class="popular-service__body"><?=$service->name?></div>
    </a>
</div>

