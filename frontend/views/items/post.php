<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $post yeesoft\post\models\Post */

$page = (isset($page)) ? $page : 'post';
?>

<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="news-item">
        <h4 class="news-item__title"><?= Html::a($post->title, ["/site/{$post->slug}"]) ?></h4>
        <div class="news-item__body text-gray-light">
            <?= $post->getThumbnail(['class' => 'thumbnail pull-left', 'style' => 'width: 160px; margin: 0 7px 7px 0']) ?>
            <?= ($page === 'post') ? $post->content : $post->shortContent ?>
        </div>
        <div class="news-item__bottom margin-top">
            <div class="row margin-bottom">
                <div class="col-xs-6">
                    <?php if ($post->category): ?>
                        <b><?= Yii::t('yee/post', 'Posted in') ?></b>
                        <a href="<?= Url::to(['/category/index', 'slug' => $post->category->slug]) ?>">"<?= $post->category->title ?>"</a>
                    <?php endif; ?>
                </div>
                <div class="col-xs-6 text-right news-item__tags">
                    <?php $tags = $post->tags; ?>
                    <?php if (!empty($tags)): ?>
                        <b><?= Yii::t('yee/post', 'Tags') ?>:</b>
                        <?php foreach ($tags as $tag): ?>
                            <?= Html::a('#' . $tag->title, ['/tag/index', 'slug' => $tag->slug]) ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                <span ><?= Yii::t('yee', 'Published') ?> by
                    <b><?= $post->author ? $post->author->username : 'Admin' ?></b> on
                    <b><?= $post->publishedDate ?></b>
                </span>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?= Url::to(["/site/{$post->slug}"])?>" class="btn btn-white-border btn-small"><?=Yii::t('app', 'More');?> </a>
                </div>
            </div>
        </div>
    </div>
</div>

