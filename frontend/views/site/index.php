<?php

use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = 'Homepage';
?>


<div class="main-categories">
    <a href="#" class="main-categories__item">
        <i class="ion-ios-location-outline"></i>
        <span>Миграция</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-ios-lightbulb-outline"></i>
        <span>Ком. услуги</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-social-euro-outline"></i>
        <span>Бизнес</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-ios-book-outline"></i>
        <span>Образование</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-ios-briefcase-outline"></i>
        <span>Работа</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-ios-medkit-outline"></i>
        <span>Медицина</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-ios-information-outline"></i>
        <span>Юр. помощь</span>
    </a>
    <a href="#" class="main-categories__item">
        <i class="ion-ios-calculator-outline"></i>
        <span>Налоги</span>
    </a>
</div>
<div class="main-search">
    <div class="container">
        <h1 class="text-center"><?=Yii::t('app', 'Cyprus Services');?> </h1>
        <form>
            <div class="form-input-group">
                <input type="text" placeholder="<?=Yii::t('app', 'Find service');?> " />
                <button class="btn btn-success">
                    <i class="ion-ios-search"></i>
                </button>
            </div>
        </form>
    </div>
</div>
<div class="container-box">
    <h3><?=Yii::t('app','Popular services')?></h3>
    <div class="row margin-top-large">
        <?php /* @var $services \common\models\Service[] */ ?>
        <?php foreach ($services as $service) : ?>
            <?= $this->render('/items/service', ['service' => $service]) ?>
        <?php endforeach; ?>
    </div>
</div>
<div class="news-index">
    <div class="container-box">
        <div class="row">
            <div class="col-xs-12 text-center">
                            <h2 class="text-center"><?=Yii::t('app','News')?></h2>
            </div>

            <?php /* @var $posts yeesoft\post\models\Post[] */ ?>
            <?php foreach ($posts as $post) : ?>
                <?= $this->render('/items/post.php', ['post' => $post, 'page' => 'index']) ?>
            <?php endforeach; ?>
            <div class="col-xs-12 text-center margin-top-large">
                <a href="#" class="btn btn-success"><?=Yii::t('app', 'All news');?> </a>
            </div>
        </div>
    </div>
</div>





