<?php

namespace frontend\components;
use common\models\CMS;
use common\models\Municipal;
use common\models\Order;
use common\models\OrderMoney;
use common\models\ServiceModule;
use Mpdf\Mpdf;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 *
 * @inheritdoc
 *
 * @property mixed $id
 * @property Order $order
 * @property mixed $order_id
 * @property int $paid
 * @property array $pricePerType
 * @property void $collectData
 * @property mixed $service
 * @property string $documentName
 * @property string $documentPath
 * @property bool $isCompleted
 * @property bool $isActive
 * @property bool $isModerated
 * @property array $orderList
 * @property int $type
 */
class Service extends Component

{
    public $isPaid = false;
    private $current_type;
    private $current_order;
    private $current_service;
    /** @var \common\models\Service */
    private $service_model;
    private $total_paid;


    public $_is_paid_by_type = [];
    public $default_type = 1;
    /**
     * @var $order_model Order
     */
    private $order_model;

    public function init(){
        $this->loadFromCookie();
        $this->loadOrder();

        $this->service_model = \common\models\Service::findOne($this->id);
        if($this->current_type == null) $this->current_type = $this->default_type;

        $this->isPaid = $this->paid  >= $this->pricePerType[$this->current_type] ? true : false;

    }

    public function loadFromCookie($serv_id = false){
        $cookies = Yii::$app->request->cookies;
        $this->current_service = $cookies->getValue('service_id', null);

        if($serv_id) $this->current_service = $serv_id;
        $cookie_types = json_decode($cookies->getValue('tps_srv', ''), true);
        $this->current_type = isset($cookie_types[$this->current_service]) ? $cookie_types[$this->current_service] : null;
    }

    public function loadOrder(){
        $model = $this->order_model = Order::findForUserByService($this->current_service);
        if(!$model) return false;
        $this->current_order = $model->id;
        $this->current_type = $model->type_id;
    }

    public function setType($type){
        $this->current_type = $type;

        $cookies_req = Yii::$app->request->cookies;
        $cookie_types = json_decode($cookies_req->getValue('tps_srv', ''), true);

        $cookie_types[$this->current_service] = $type;

        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'tps_srv',
            'value' => json_encode($cookie_types),
//                'expire' => time() + 3600
        ]));

        if($this->order_model == null) {

            return false;
        }

        $this->order_model->type_id = $type;
        $this->order_model->save();
    }

    public function getType(){
        return $this->current_type;
    }

    /** Назначить ордер
     * @param $order
     */
    public function setOrder($order){
        $this->current_order = $order;

        if($this->order_model){
            $this->order_model->active = 0;
            $this->order_model->save();
        }

        if($this->current_order){
            $model = Order::findOne($order);
            $model->active = 1;
            $model->save();
            $this->order_model = $model;
        }

    }

    /**
     * @return float|int
     */
    public function getPaid(){
        if(!$this->total_paid){
            $this->total_paid = $this->order->paid;
        }

        return $this->total_paid;
    }

    /**
     * @return Order
     */
    public function getOrder(){
        return $this->order_model;
    }

    /**
     * @return integer
     */
    public function getOrder_id(){
        return $this->current_order;
    }

    /**
     * @return integer
     */
    public function getId(){
        return $this->current_service;
    }

    /** Назначить сервис
     * @param $id
     */
    public function setService($id){
        $id = (int) $id;
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'service_id',
            'value' => $id,
            'expire' => time() + 3600*24*7
        ]));
        $this->current_service = $id;
        $this->loadFromCookie($id);
    }

    /** Оплатить
     * @param $price
     */
    public function pay($price){
        $model = new OrderMoney();
        $model->price = $price;
        $model->status = 1;

        $model->user_id = Yii::$app->user->id;
        $model->order_id = $this->order->id;

        $model->save();
    }

    /** Получить цену по типу
     * @return array
     */
    public function getPricePerType(){
        return $this->service_model->pricePerType;
    }

    /** Собрать данные
     * @return bool
     * @throws \Exception
     */
    public function getCollectData(){
        $steps = $this->service_model->stepsByType($this->type);

        $data = [];
        foreach ($steps as $step){
            if($step->module_name == ServiceModule::MODULE_FORM){
                $data[] = $this->collectForm();
            }
            elseif ($step->module_name == ServiceModule::MODULE_MUNICIPAL){
                $data[] = $this->collectMunicipal();
            }
        }

        $fname = $this->order_model->number.'.zip';
        $file = Yii::getAlias('@tmp').'/'.Yii::$app->user->id.'/'.$fname;
        CMS::createZip($data, $file);
        CMS::downloadFile($file, $fname);
        $this->order_model->status = Order::STATUS_COMPLETE;
        $this->order_model->save();
        return true;
    }

    /** Сгенерировать файл формы
     * @return string
     * @throws \Mpdf\MpdfException
     */
    private function collectForm(){
        $isPaid = $this->isPaid;
        $model = $this->service_model;
        $content = Yii::$app->view->render('//service/document', compact('model', 'isPaid'));

        $pdf = new Mpdf();
        $stylesheet = file_get_contents(Yii::getAlias('@frontend/web/css/form_pdf.css'));
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($content, 2);
        $dir = Yii::getAlias('@host').'/tmp/'.Yii::$app->user->id.'/';
        CMS::checkDir($dir);
        $pdf->Output($dir.'documents.pdf', 'F');
        return $dir.'documents.pdf';
    }

    /** Сгенерировать муниципальные учреждения
     * @return string
     * @throws \Mpdf\MpdfException
     */
    private function collectMunicipal(){
        $order_data = $this->order->getOrderModules()->where(['module_name' => ServiceModule::MODULE_MUNICIPAL])->one();

        $municipal_id = [];
        foreach ($order_data->data as $k => $v){
            if(!$v) continue;
            $municipal_id[] = $k;
        }
        $model = Municipal::find()->where(['id' => $municipal_id])->all();
        $content = Yii::$app->view->render('//service/pdf/municipal', compact('model'));


        $pdf = new Mpdf([
//            'mode' => 'utf-8',
//            'format' => [190, 236],
        ]);
        $stylesheet = file_get_contents(Yii::getAlias('@frontend/web/css/page_pdf.css'));
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($content, 2);
//        echo $content;
        $dir = Yii::getAlias('@host').'/tmp/'.Yii::$app->user->id.'/';
        CMS::checkDir($dir);
        $pdf->Output( $dir.'municipal.pdf', 'F');
        return $dir.'municipal.pdf';
    }

    /** Get generated document name
     * @return string
     */
    public function getDocumentName(){
        return $this->order_model->number.'.zip';
    }

    /** Get generated document path
     * @return string
     */
    public function getDocumentPath(){
        return Yii::getAlias('@tmp').'/'.Yii::$app->user->id.'/'.$this->documentName;
    }

    /** Order is active
     * @return bool
     */
    public function getIsActive(){
        return $this->order_model->status == Order::STATUS_PROCESS;
    }

    /** Order is complete
     * @return bool
     */
    public function getIsCompleted(){
        return $this->order_model->status == Order::STATUS_COMPLETE;
    }

    /** Order on moderate
     * @return bool
     */
    public function getIsModerated(){
        return $this->order_model->status == Order::STATUS_MODERATE;
    }

    /** Get Order List
     * @return array
     */
    public function getOrderList(){
        $model = Order::findAllByService($this->service_model->id);

        return ArrayHelper::map($model, 'id', 'numberFull');
    }
}