<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 19.03.2019
 * Time: 15:50
 */

namespace frontend\modules\auth\controllers;


use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class DefaultController extends \yeesoft\auth\controllers\DefaultController
{

    public function behaviors()
    {
        $parents = parent::behaviors();
        unset($parents['verbs']['actions']['logout']);
        return $parents;
    }
}