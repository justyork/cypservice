<?php

namespace app\models;

use yeesoft\models\User;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_form".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $user_id
 *
 * @property User $_user
 */
class UserForm extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_form';
    }

    /**
     * @return UserForm[]|array|ActiveRecord[]
     */
    public static function findByCurrentUser()
    {
        return self::find()->where(['user_id' => Yii::$app->user->id])->all();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get_user()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
