<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 09.12.2017
 * Time: 17:46
 */

namespace frontend\models;


use yii\helpers\Html;

class PDF{

    public $debug = false;

    public function v($val, $top, $left, $w = 200, $h = 20){
        return Html::tag('div', $val, [
            'class' => 'item_val',
            'style' => '
                margin-left: '.$left.';
                margin-top: '.$top.';
                width: '.$w.';
                height: '.$h.';
            '
        ]);
    }

    public function clear($height = 1){
        return '<div class="clear" style="height: '.$height.'px;"></div>';
    }

    public function blockStart($width, $height){
        return '<div style="width: '.$width.'px; height: '.$height.'px;" class="block">';
    }

    public function blockEnd(){
        return '</div>';
    }

    public function row($val){
        $val = "<div>{$val}</div>";
        return $val;
    }
    public function lst($name, $value){
        $val = "<table><tr><td style=''><b>{$name}:</b> {$value}</td></tr></table>";
        return $val;
    }
}