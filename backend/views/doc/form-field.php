<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 22.11.2017
 * Time: 14:19
 */


$data = [
    ['name' => 'name', 'desc' => 'Имя', 'type' => 'String', ],
    ['name' => 'surname', 'desc' => 'Фамилия', 'type' => 'String', ],
    ['name' => 'middle_name', 'desc' => 'Отчество / Второе имя', 'type' => 'String', ],
    ['name' => 'nationality', 'desc' => 'Национальность', 'type' => 'String', ],
    ['name' => 'date_birth', 'desc' => 'Дата рождения', 'type' => '', ],
    ['name' => 'place_birth', 'desc' => 'Место рождения', 'type' => 'String', ],
    ['name' => 'country', 'desc' => 'Страна', 'type' => 'String', ],
    ['name' => 'marital_status', 'desc' => 'Семейное положение', 'type' => '', ],
    ['name' => 'number_of_marriage', 'desc' => 'Номер сертификата для полигамных браков', 'type' => 'String', ],
    ['name' => 'traver_doc_no', 'desc' => 'Номер документа по которому прибыли', 'type' => 'String', ],
    ['name' => 'traver_doc_place', 'desc' => 'Место выдачи', 'type' => 'String', ],
    ['name' => 'traver_doc_expire', 'desc' => 'Дата окончания действия док', 'type' => '', ],
    ['name' => 'port_entry', 'desc' => 'Порт приезда', 'type' => 'String', ],
    ['name' => 'date_entry', 'desc' => 'Дата приезда', 'type' => '', ],
    ['name' => 'gender', 'desc' => 'Пол', 'type' => '', ],
    ['name' => 'arc', 'desc' => '', 'type' => '', ],
    ['name' => 'residence_status', 'desc' => '', 'type' => '', ],
    ['name' => 'residence_date_issue', 'desc' => '', 'type' => '', ],
    ['name' => 'residence_date_expire', 'desc' => '', 'type' => '', ],
    ['name' => 'home_phone', 'desc' => 'Домашний телефон', 'type' => '', ],
    ['name' => 'office_phone', 'desc' => 'Офисный телефон', 'type' => '', ],
    ['name' => 'mobile_phone', 'desc' => 'Мобильный телефон', 'type' => '', ],
    ['name' => 'fax', 'desc' => 'Факс', 'type' => '', ],
    ['name' => 'email', 'desc' => 'эл. Почта', 'type' => '', ],
    ['name' => 'address_street', 'desc' => 'Название улицы по месту пребывания', 'type' => '', ],
    ['name' => 'address_area', 'desc' => 'Название района', 'type' => '', ],
    ['name' => 'address_city', 'desc' => 'Название города', 'type' => '', ],
    ['name' => 'address_postal', 'desc' => 'Почтовый индекс', 'type' => '', ],
    ['name' => 'abroad_street', 'desc' => 'Улица и номер дома за границей (домашний)', 'type' => '', ],
    ['name' => 'abroad_city', 'desc' => 'Город заграницей', 'type' => '', ],
    ['name' => 'abroad_country', 'desc' => 'Страна заграницей', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],
    ['name' => '', 'desc' => '', 'type' => '', ],

];


?>

<h2><?=Yii::t('app', 'Form fields description');?> </h2>


<div class="panel panel-default">
    <div class="panel-body">
        <?foreach ($data as $item):?>
            <h3><?=$item['name']?></h3>
            <p><?=$item['desc']?></p>
            <div><b><?=Yii::t('app', 'Type');?>: <?=$item['type']?> </b></div>
            <hr>
        <?endforeach;?>
    </div>
</div>
