<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $form yeesoft\widgets\ActiveForm */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yeesoft\helpers\Html;
use yeesoft\widgets\ActiveForm;
use yeesoft\widgets\LanguagePills;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yeesoft\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin([
        'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form',
        'validateOnBlur' => false,
    ]); ?>

    <div class="row">

        <div class="col-md-9">

            <div class="panel panel-default">
                <div class="panel-body">

                    <?= "<?php " ?> if ($model->isMultilingual()): ?>
                        <?= "<?=" ?> LanguagePills::widget() ?>
                    <?= "<?php " ?> endif; ?>

                    <?php foreach ($generator->getColumnNames() as $attribute) {
                        if (in_array($attribute, $safeAttributes)) {
                            echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                        }
                    } ?>

                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="record-info">
                        <?php if (!$model->isNewRecord): ?>


                        <?php endif; ?>

                        <div class="form-group">
                            <?= "<?php " ?> if ($model->isNewRecord): ?>
                                <?= "<?=" ?> Html::submitButton(Yii::t('yee', 'Create'), ['class' => 'btn btn-primary']) ?>
                                <?= "<?=" ?> Html::a(Yii::t('yee', 'Cancel'), ['index'], ['class' => 'btn btn-default',]) ?>
                            <?= "<?php " ?> else: ?>
                                <?= "<?=" ?> Html::submitButton(Yii::t('yee', 'Save'), ['class' => 'btn btn-primary']) ?>
                                <?= "<?=" ?>
                                Html::a(Yii::t('yee', 'Delete'), ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-default',
                                    'data' => [
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ])
                                ?>
                            <?= "<?php " ?> endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="record-info">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>
</div>
