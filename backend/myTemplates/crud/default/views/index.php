<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();



echo "<?php\n";
?>

use yii\helpers\Url;
use yeesoft\grid\GridPageSize;
use yeesoft\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yeesoft\\grid\\GridView" : "yeesoft\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="lte-hide-title page-title"><?= "<?= " ?>Html::encode($this->title) ?></h3>
                <?= "<?= " ?> Html::a(Yii::t('yee', 'Add New'), ['create'], ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-6">
                    </div>

                    <div class="col-sm-6 text-right">
                        <?= "<?= " ?> GridPageSize::widget(['pjaxId' => 'page-grid-pjax']) ?>
                    </div>
                </div>

                <?= $generator->enablePjax ? '<?php Pjax::begin(); ?>' : '' ?>
                <?php if ($generator->indexWidgetType === 'grid'): ?>
                    <?= "<?= " ?>GridView::widget([
                        'dataProvider' => $dataProvider,
                        'bulkActionOptions' => [
                            'gridId' => 'page-grid',
                            'actions' => [
                                Url::to(['bulk-delete']) => Yii::t('yee', 'Delete'),
                            ]
                        ],
                        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
                            ['class' => 'yeesoft\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                            /*[
                                'class' => 'yeesoft\grid\columns\TitleActionColumn',
                                'controller' => '<?= $generator->controllerID?>/<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>',
                                'title' => function (<?= ltrim($generator->modelClass, '\\') ?> $model) {
                                    return Html::a($model->name, ['view', 'id' => $model->id], ['data-pjax' => 0]);
                                },
                            ],*/

                <?php
                $count = 0;
                if (($tableSchema = $generator->getTableSchema()) === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        if (++$count < 6) {
                            echo "            '" . $name . "',\n";
                        } else {
                            echo "            // '" . $name . "',\n";
                        }
                    }
                } else {
                    foreach ($tableSchema->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        if (++$count < 6) {
                            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        } else {
                            echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        }
                    }
                }
                ?>

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                <?php else: ?>
                    <?= "<?= " ?>ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemOptions' => ['class' => 'item'],
                        'itemView' => function ($model, $key, $index, $widget) {
                            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                        },
                    ]) ?>
                <?php endif; ?>
                <?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
        </div>
    </div>
</div>
