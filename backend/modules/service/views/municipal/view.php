<?php

use yeesoft\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Municipal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Municipals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="municipal-view">

    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>

    <div class="panel panel-default">
        <div class="panel-body">
            <?=  Html::a(Yii::t('yee', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?=  Html::a(Yii::t('yee', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-default',
                'data' => [
                    'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?=  Html::a(Yii::t('yee', 'Add New'), ['create'], ['class' => 'btn btn-sm btn-primary pull-right']) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <h2></h2>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                                'id',
            'phone',
            'name',
            'email:email',
            'website',
            'logo',
            'address:ntext',
            'lat',
            'lng',
            'work_time:ntext',
            'city_id',
                ],
            ]) ?>
        </div>
    </div>

</div>
