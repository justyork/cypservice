<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Municipal */

$this->title = Yii::t('app', 'Create Municipal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Municipals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="municipal-create">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
