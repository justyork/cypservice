<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Municipal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Municipal',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Municipals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="municipal-update">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
