<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CheckListItem */

$this->title = Yii::t('app', 'Create Check List Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Check List Items'), 'url' => ['check-list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-list-item-create">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
