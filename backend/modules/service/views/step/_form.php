<?php

use yeesoft\widgets\LanguagePills;
use yeesoft\helpers\Html;
use yeesoft\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceStep */
/* @var $form yii\widgets\ActiveForm */

$module = new \common\models\ServiceModule($model->module_name, $model->id);


?>

<div class="service-step-form">

    <?php $form = ActiveForm::begin([
        'id' => 'service-step-form',
        'validateOnBlur' => false,
    ]); ?>

    <div class="row">

        <div class="col-md-9">

            <div class="panel panel-default">
                <div class="panel-body">

                    <?php if ($model->isMultilingual()): ?>
                        <?= LanguagePills::widget() ?>
                    <?php endif; ?>

                    <?= $form->errorSummary($model)?>

                    <?= $form->field($model, 'service_id')->dropDownList(\common\models\Service::getList(), ['prompt' => Yii::t('app', '-- none --')]) ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'step')->textInput()->hint(Yii::t('app', 'Use 0 for an empty point or any other number')) ?>


                    <?= $form->field($model, 'type')->textInput(['maxlength' => true])->hint(Yii::t('app', 'Show step in types [1,3,5]')) ?>

                    <?= $form->field($model, 'position')->textInput() ?>

                    <?= $form->field($model, 'module_name')->dropDownList(\common\models\ServiceModule::getList(), [
                        'onchange' => '
            $.post( "' . \yii\helpers\Url::to(['ajax/load-module', 'id' => $model->id]) . '",{module_name:$(this).val()}, function( ret ) {
                $( ".module-id" ).html( ret );
            });
        '
                    ]) ?>

                    <div class="module-id">
                        <?= $module->load() ?>
                    </div>
                    
                    <?= $form->field($model, 'status')->checkbox() ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'text_bottom')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="record-info">

                        <div class="form-group">
                            <?php if ($model->isNewRecord): ?>
                                <?= Html::submitButton(Yii::t('yee', 'Create'), ['class' => 'btn btn-primary']) ?>
                                <?= Html::a(Yii::t('yee', 'Cancel'), ['index'], ['class' => 'btn btn-default',]) ?>
                            <?php else: ?>
                                <?= Html::submitButton(Yii::t('yee', 'Save'), ['class' => 'btn btn-primary']) ?>
                                <?= Html::a(Yii::t('yee', 'Delete'), ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-default',
                                    'data' => [
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ])
                                ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
