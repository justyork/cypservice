<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceStep */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Service Step',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service Steps'), 'url' => ['default/view', 'id' => $model->service_id]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="service-step-update">

    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
