<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ServiceStep */

$this->title = Yii::t('app', 'Create Service Step');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service Steps'), 'url' => ['default/view', 'id' => $model->service_id]];
$this->params['breadcrumbs'][] = $this->title;

if(isset($_GET['id'])) $model->service_id = $_GET['id'];
?>
<div class="service-step-create">

    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
