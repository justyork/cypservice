<?php

use common\models\ServiceStep;
use yeesoft\grid\GridPageSize;
use yeesoft\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Service */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-view">

    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= Html::a(Yii::t('yee', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a(Yii::t('yee', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-default',
                'data' => [
                    'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a(Yii::t('yee', 'Add New'), ['create'], ['class' => 'btn btn-sm btn-primary pull-right']) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'icon',
                    'color:color',
                    'description:ntext',
                    'time',
                    'cost',
                    'status:status',
                    'is_main:boolean',
                    'category_id',
                    'code',
                ],
            ]) ?>

        </div>
    </div>

    <h3 class="lte-hide-title"><?= Yii::t('app', 'Service Steps') ?></h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= Html::a(Yii::t('yee', 'Add New'), ['step/create', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary pull-right']) ?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                </div>

                <div class="col-sm-6 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'post-grid-pjax']) ?>
                </div>
            </div>

            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'bulkActionOptions' => [
                    'gridId' => 'post-grid',
                    'actions' => [
                        Url::to(['bulk-activate']) => Yii::t('yee', 'Publish'),
                        Url::to(['bulk-deactivate']) => Yii::t('yee', 'Unpublish'),
                        Url::to(['bulk-delete']) => Yii::t('yii', 'Delete'),
                    ]
                ],
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yeesoft\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'class' => 'yeesoft\grid\columns\TitleActionColumn',
                        'controller' => '/service/step',
                        'buttons' => [
                            'duplicate' => function ($url, ServiceStep $model) {
                                return Html::a(Yii::t('yee', 'Duplicate'), ['step/duplicate', 'id' => $model->id]);
                            }
                        ],
                        'buttonsTemplate' => '{update} {duplicate} {view} {delete} ',
                        'title' => function (ServiceStep $model) {
                            return Html::a($model->title, ['step/view', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                    ],
                    'type',
                    'step',
                    'module_name',
                    'position',
                    'status:status',

                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
