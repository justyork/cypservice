<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Fork */

$this->title = Yii::t('app', 'Create Fork');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fork'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">
    <h3 class="lte-hide-title"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', compact('model')) ?>
</div>
