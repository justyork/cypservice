<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 12.01.2018
 * Time: 17:27
 */

namespace backend\modules\service\controllers;


use yeesoft\controllers\admin\BaseController;

class ForkController extends BaseController
{
    public $modelClass = 'common\models\Fork';
    public $modelSearchClass = 'common\models\search\ForkSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }
}