<?php

namespace backend\modules\service\controllers;

use Yii;
use common\models\Municipal;
use common\models\search\MunicipalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MunicipalController implements the CRUD actions for Municipal model.
 */
class MunicipalController extends \yeesoft\controllers\admin\BaseController
{

    public $modelClass = 'common\models\Municipal';
    public $modelSearchClass = 'common\models\search\MunicipalSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }


}
