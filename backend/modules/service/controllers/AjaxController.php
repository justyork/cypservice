<?php

namespace backend\modules\service\controllers;

use common\models\ServiceModule;
use yeesoft\controllers\admin\BaseController;
use Yii;
use common\models\ServiceStep;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * StepController implements the CRUD actions for ServiceStep model.
 */
class AjaxController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function beforeAction($action){

        if (Yii::$app->request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            return parent::beforeAction($action);
        }

        throw new BadRequestHttpException();
    }


    public function actionLoadModule($id = null){

        $model = new ServiceModule($_POST['module_name'], $id);
        return [
            $model->load()
        ];


    }


}
