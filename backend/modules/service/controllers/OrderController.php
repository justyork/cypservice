<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 28.02.2018
 * Time: 13:50
 */

namespace backend\modules\service\controllers;


use common\models\Order;
use yeesoft\controllers\admin\BaseController;
use yii\data\ActiveDataProvider;

class OrderController extends BaseController
{
    public $modelClass = 'common\models\Order';
    public $modelSearchClass = 'common\models\search\CategorySearch';

    public function actionIndex(){
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find(),
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionView($id){
        $model = $this->findModel($id);
        return $this->render('view', compact('model'));
    }


}