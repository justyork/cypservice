<?php

namespace backend\modules\service\controllers;

use common\models\search\CheckListItemSearch;
use Yii;
use common\models\CheckList;
use common\models\search\CheckListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CheckListController implements the CRUD actions for CheckList model.
 */
class CheckListController extends \yeesoft\controllers\admin\BaseController
{

    public $modelClass = 'common\models\CheckList';
    public $modelSearchClass = 'common\models\search\CheckListSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }

    public function actionView($id)
    {
        $searchModel = new CheckListItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $model = $this->findModel($id);
        return $this->renderIsAjax($this->viewView, compact('model', 'searchModel', 'dataProvider'));
    }

}
