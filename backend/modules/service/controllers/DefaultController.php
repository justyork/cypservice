<?php

namespace backend\modules\service\controllers;

use common\models\ServiceStep;
use Yii;
use common\models\Service;
use yii\data\ActiveDataProvider;
use yeesoft\controllers\admin\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Service model.
 */
class DefaultController extends BaseController
{

    public $modelClass = 'common\models\Service';
    public $modelSearchClass = 'common\models\search\ServiceSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ServiceStep::find()->where(['service_id' => $id])->orderBy('position, module_id'),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }


}
