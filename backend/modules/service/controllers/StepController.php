<?php

namespace backend\modules\service\controllers;

use yeesoft\controllers\admin\BaseController;
use Yii;
use common\models\ServiceStep;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StepController implements the CRUD actions for ServiceStep model.
 */
class StepController extends BaseController
{

    public $modelClass = 'common\models\ServiceStep';
    public $modelSearchClass = 'common\models\search\ServiceStepSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }

    public function actionDuplicate($id){
        $model = ServiceStep::findOne($id);

        $new = new ServiceStep();
        $new->attributes = $model->attributes;
        $new->id = null;
        $new->save();
        return $this->redirect(['default/view', 'id' => $model->service_id]);
    }


}
