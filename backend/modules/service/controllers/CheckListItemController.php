<?php

namespace backend\modules\service\controllers;

use Yii;
use common\models\CheckListItem;
use common\models\search\CheckListItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CheckListItemController implements the CRUD actions for CheckListItem model.
 */
class CheckListItemController extends \yeesoft\controllers\admin\BaseController
{

    public $modelClass = 'common\models\CheckListItem';
    public $modelSearchClass = 'common\models\search\CheckListItemSearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }


}
