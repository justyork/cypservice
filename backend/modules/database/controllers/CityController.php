<?php

namespace backend\modules\database\controllers;

use Yii;
use common\models\City;
use common\models\search\CitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends \yeesoft\controllers\admin\BaseController
{

    public $modelClass = 'common\models\City';
    public $modelSearchClass = 'common\models\search\CitySearch';

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }


}
