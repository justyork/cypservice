<?php

namespace backend\controllers;

use yeesoft\controllers\admin\DashboardController;

class DocController extends DashboardController
{

    public function actionFormsField(){

        return $this->render('form-field');
    }

}