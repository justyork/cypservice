$(document).ready(function() {
  $('.fancybox-ifr').fancybox({
      'width'		: 900,
      'height'	: 600,
      'type'		: 'iframe',
      'autoScale'    	: false
  });
  $('.fancybox').fancybox();
});