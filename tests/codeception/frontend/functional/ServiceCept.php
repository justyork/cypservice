<?php
use tests\codeception\frontend\FunctionalTester;

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that service page works');

$I->amOnRoute('service/get', ['id' => 1]);
$I->see('Pink slip');
$I->seeElement('.service-step');